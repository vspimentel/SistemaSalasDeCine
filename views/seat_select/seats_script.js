class Seat{
    constructor(id, seat_n, state){
        this.id = id
        this.seat_n = seat_n
        this.state = state
    }
}

selected_seats = []
edit_seats = false


function createSeat(seat){
    if(seat.state == "F"){
        seat_img = `sit_free.svg`
    } else {
        seat_img = `sit_occupied.svg`
    }
    return `<div class="sit" id="sit_${seat.id}">
    <img src="../../resource/svg/${seat_img}" width="58px">
    <div class="sit_number">${seat.seat_n}</div>
    
    </div>`
}

function createHall(hall){
    var table = `<table class="sits_table">`
    for (let i = 0; i < hall.rows+1; i++) {
        table += `<tr>`
        for (let j = 0; j < hall.columns+1; j++) {
            if(i == 0 && j == 0){
                table += `<th style="padding: 15px;"></th>`
            }else if(i == 0){
                table += `<th class="th_colums">${j}</th>`
            }else if(j == 0){
                table += `<th class="th_rows">${String.fromCharCode((64+hall.rows+1)-i)}</th>`
            }else{
                table += `<td>${createSeat(hall.seats[(i-1)*hall.columns+j-1])}</td>`
            }
        }
        table += `</tr>`
    }
    table += `<tr>
                <th> </th>
                <td colspan="${hall.columns}" class="screen">Pantalla</td>
            </tr>`
    table += `</table>`
    return table
}

function seatHover(element, hall){
    var img = element.getElementsByTagName("img")[0];
    var id = element.id.split("_")[1]
    var seat = hall.seats.find(seat => seat.id == id)
    if(seat.state == "F"){
        img.src = "../../resource/svg/sit_selected.svg";
    }
}

function seatLeave(element, hall){
    var img = element.getElementsByTagName("img")[0];
    var id = element.id.split("_")[1]
    var seat = hall.seats.find(seat => seat.id == id)
    if(seat.state == "F"){
        img.src = "../../resource/svg/sit_free.svg";
    }
}

function selectSeat(element, hall){
    var id = element.id.split("_")[1]
    var seat = hall.seats.find(seat => seat.id == id)
    if(seat.state == "F"){
        seat.state = "S";
        selected_seats.push(seat)
    } else if(seat.state == "S"){
        seat.state = "F";
        selected_seats = selected_seats.filter(seat => seat.id != id)
    }
}


function setSeatsFunction(){
    var seats = document.getElementsByClassName("sit");
    for (let i = 0; i < seats.length; i++) {
        seats[i].onmouseenter = function() {seatHover(this, hall)};
        seats[i].onmouseleave = function() {seatLeave(this, hall)};
        seats[i].onclick = function() {selectSeat(this, hall)};
        if(seats[i].getElementsByTagName("img")[0].src.includes("sit_occupied.svg")){
            seats[i].style.pointerEvents = "none";
        }
    }
}

function editSeats(){
    var seats = document.getElementsByClassName("sit");
    for (let i = 0; i < seats.length; i++) {
        var id = seats[i].id.split("_")[1]
        if(selected_seats.find(seat => seat.id == id)){
            seat = hall.seats.find(seat => seat.id == id)
            seat.state  = "S"
            seats[i].getElementsByTagName("img")[0].src = "../../resource/svg/sit_selected.svg"
        } else if ((seat = hall.seats.find(seat => seat.id == id)).state  != "O"){
            seat.state  = "F"
            seats[i].getElementsByTagName("img")[0].src = "../../resource/svg/sit_free.svg"   
        }
    }
    edit_seats = false
}

function finishSelection(){
    ticket_list = []
    selected_seats.forEach(seat => {
       t = new Ticket(last_ticket_id, seats_data[2], seats_data[1], seats_data[0], seat)
       last_ticket_id++
       ticket_list.push(t)
    });
    updateTicket()
    updatePrice()
    content = document.getElementById("content")
    content.innerHTML = `<h1>Cartelera</h1>
    <div id="movies">
    </div>`
    loadMovies()
}

function updateSeatState(seat, state, day){
    fetch(`../../db/editSeat.php?seat=${seat.id}&state=${state}&schedule=0&day=${day}`).
    then(response => response.text())
    .then(data => {

    })
}