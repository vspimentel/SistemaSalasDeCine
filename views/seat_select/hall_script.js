class Hall{
    constructor(id, number, rows, columns, seats){
        this.id = id
        this.number = number
        this.rows = rows
        this.columns = columns
        this.seats = seats
    }
}

today = new Date()
day = today.getDay()
seats_data = []

function fetchSchedule(schedule_id, movie_id, edit){
    if(schedule_id == null || movie_id == null){
        ticket = ticket_list[0]
        schedule_id = ticket.schedule.id
        movie_id = ticket.movie.id
    }
    fetch(`../../db/fetchQuery.php?query=schedule&id=${schedule_id}`)
    .then(response => response.text())
    .then(data => {
        response = JSON.parse(data);
        if(response.length) {
            $.each(response, function(key,value) {
                movie = movie_list.find(movie => movie.id == movie_id)
        		hall = new Hall(value.IDSALA, value.NOMBRE_SALA, value.FILAS, value.COLUMNAS, [])
                schedule = movie.schedule.find(s => s.id == schedule_id)
                seats_data = [hall, schedule, movie]
                loadSeats(hall, schedule, movie)
                edit_seats = edit
        	});
        }
    });
}

function loadSeats(hall, schedule, movie){
    content = document.getElementById("content")
    content.innerHTML = `
    <div style="display: flex; justify-content: space-between; align-items: center;">
        <h1 style="margin-bottom: 0;">Selección de butaca</h1>
        <div class="button_1" onclick="freeSeats()">Liberar asientos</div>
    </div>
    <h1 style="margin-bottom: 0;">${movie.title} - ${movie.audio} - ${movie.video}</h1>
    <div style="display: flex; justify-content: space-between; align-items: center;">
        <h1 style="font-size: 28px">${hall.number} - ${schedule.time}</h1>
        <div style="display: flex; gap: 30px;">
                        <div class="input-wrapper">
                            <label for="video" class="label_select" style="left: 10px; top: -8px;">Dia</label>
                            <select name="video" class="input_select" id="input_day">
                                <option value="1">Lunes</option>
                                <option value="2">Martes</option>
                                <option value="3">Miércoles</option>
                                <option value="4">Jueves</option>
                                <option value="5">Viernes</option>
                                <option value="6">Sábado</option>
                                <option value="0">Domingo</option>
                            </select>
                        </div>
                        <div class="button_1" onclick="finishSelection()">Terminar selección</div>
                    </div>
    </div>
    <div id="table">
    </div>`
    date_input = document.getElementById("input_day")
    date_input.value = day
    date_input.onchange = function(){
        day = date_input.value
        fetchSeats(hall, schedule, day)
    }
    fetchSeats(seats_data[0], seats_data[1]);
}

function setSeatsN(hall){
    for (let i = 0; i < hall.rows; i++) {
        for (let j = 0; j < hall.columns; j++) {
            hall.seats[(i*hall.rows)+j].seat_n = String.fromCharCode((64+hall.rows)-i)+(j+1)
        }
    }
}

function fetchSeats(hall, schedule){
    table = document.getElementById("table")
    fetch(`../../db/fetchQuery.php?query=seats&idfuncion=${schedule.id}&day=${day}`).then(response => response.text()).then(data => {
        var response = JSON.parse(data)
        hall.seats = []
        for (let i = 0; i < response.length; i++) {
            seat = new Seat(response[i].IDBUTACA, "", response[i].ESTADO)
            hall.seats.push(seat)
        }

        setSeatsN(hall)
        table.innerHTML = createHall(hall)
        setSeatsFunction()
        if(edit_seats){
            editSeats()
        } else {
            ticket_list = []
            updateTicket()
            updatePrice()
            selected_seats = []
        }
    })
}

function freeSeats(){
    if(confirm("¿Desea liberar todos los asientos seleccionados de esta función?")){
        fetch(`../../db/editSeat.php?seat=0&state=F&schedule=${seats_data[1].id}&day=${day}`).
    then(response => response.text())
    .then(data => {
        hall.seats = []
        fetchSeats(hall, seats_data[1])
    })
    }
}