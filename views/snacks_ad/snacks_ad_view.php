<?php
    session_start();
    if(!isset($_SESSION["username"]) || $_SESSION["isadmin"] == 0){
        header("Location: ../unauthorized/unauthorized_view.html");
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inventario</title>
    <link rel="stylesheet" href="../page_styles.css">
    <link rel="stylesheet" href="snacks_ad_styles.css">
</head>
<body>
    <div class="container">
        <div class="lat_menu">
            <div class="menu_opt">
                <a href="../cartelera_ad/cartelera_ad_view.php" style="height:30px;"><img
                        src="../../resource/svg/movie.svg" height="30px"></a>
            </div>
            <div class="menu_opt_selected">
                <a href="#" style="height:30px;"><img src="../../resource/svg/food.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../reporte/reporte_view.php" style="height:30px;"><img src="../../resource/svg/stats.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../user_list/user_list_view.php" style="height:30px;"><img src="../../resource/svg/user_list.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../cartelera/cartelera_view.php" style="height:30px;"><img src="../../resource/svg/user1.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <img src="../../resource/svg/log-out.svg" height="30px" onclick="logOut()">
            </div>
        </div>
        <div class="content">
            <h1>Inventario</h1>
            <div class="report_settings">
                <div class="report_date">
                    <div class="date_picker">
                        <div class="date_picker_title">Desde</div>
                        <input type="date" id="date1" style="border-radius: 4px; padding-left: 3px;border-color: white;">
                    </div>
                    <div class="date_picker">
                        <div class="date_picker_title">Hasta</div>
                        <input type="date" id="date2" style="border-radius: 4px; padding-left: 3px;border-color: white;">
                    </div>
                </div>
            </div>
            <div id="inventario">
                <table id="registros_table">
                    
                </table>
                <div class="button_1" onclick="fetchRegisters()" id="execute_button"">Mostrar registros</div>
            </div>
        </div>
        <div class="edit_form">
            <h1 style="margin-bottom: 10px;">Registrar Entrada</h1>
            <div class="input-wrapper" style="margin-bottom: 5px;">
                <label for="video" class="label_select" style="left: 25px;">Producto</label>
                <select name="video" class="input_select" style="height: 40px; width: 100%;" id="productos" onchange="setInsertForm()">
                    
                </select>
            </div>
            <div class="cur_stock" id="cur_stock">Stock actual de Coca Cola: 20L</div>
            <table id="registros_producto">
                <tr>
                    <th style="width:100px">Fecha</th>
                    <th style="width:50px">Entrada</th>
                    <th style="width:50px">Salida</th>
                    <th style="width:50px">Saldo</th>
                </tr>
            </table>
            <div style="display: flex; width: 280px; justify-content: space-between;">
                <div class="input-wrapper" style="width: 240px;">
                    <label for="time" class="label_text_time">Cantidad</label>
                    <input type="text" name="time" class="input_text_time" value="0L" readonly="true" id="cantidad" style="width: 240px; margin: 10px 0">
                </div>
                <div style="display: flex; flex-direction: column;">
                    <div class="pm_button" style="margin-top: 10px;" id="p_button">+</div>
                    <div class="pm_button" style="margin-top: 5px;" id="m_button">-</div>
                </div>
            </div>
            <div class="input-wrapper">
                <label for="time" class="label_text_time" style="left: 35px;">Fecha</label>
                <input type="date" name="time" class="input_text_time" id="fecha" style="width: 280px; height: 45px;">
            </div>
            <div style="display: flex; justify-content: flex-end; width: 280px; margin-top: 5px;">
                <div class="button" onclick="insertRegister()" id="execute_button">Registrar ingreso</div>
            </div>
        </div>
    </div>
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="reporte_script.js"></script>
<script src="stock_form_script.js"></script>
<script src="../login/login_script.js"></script>
<script>
    setCurDate()
    fetchRegisters()
    fetchProductos()
</script>
</html>