// Define a Reporte class that takes in various parameters
class Reporte {
    constructor(id, producto, hora, unidad, saldo_i, entrada, salida, saldo) {
      this.id = id;
      this.producto = producto;
      this.hora = hora;
      this.unidad = unidad;
      this.saldo_i = saldo_i;
      this.entrada = entrada;
      this.salida = salida;
      this.saldo = saldo;
    }
  }
  
  // Initialize an empty array to hold report objects
  reportes_list = [];

// This function makes a request to fetch data from a database using a PHP file
// The function takes the start and end date parameters from two HTML input elements with IDs "date1" and "date2"
function fetchRegisters() {
    var start_date = document.getElementById("date1").value;
    var end_date = document.getElementById("date2").value;
  
    // Use fetch API to make a request to the PHP file with the start and end dates as parameters
    fetch(
      `../../db/fetchQuery.php?query=reports&start=${start_date}&end=${end_date}`
    )
      .then((response) => response.text())
      .then((data) => {
        // Parse the response data as JSON
        response = JSON.parse(data);
        // Empty the reportes_list array
        reportes_list = [];
        // If there is data in the response, loop through it and create a Reporte object for each item
        if (response.length) {
          $.each(response, function (key, value) {
            reporte = new Reporte(
              value.IDREGISTRO,
              value.nombre,
              value.HORA,
              value.unidad,
              value.SALDOANT,
              value.ENTRADAPROD,
              value.SALIDAPROD,
              value.SALDOPROD
            );
            // Add the new Reporte object to the reportes_list array
            reportes_list.push(reporte);
          });
        }
        // Call the loadRegisters function to display the report data in a table
        loadRegisters();
      });
  }
  

  function setCurDate(){
    // get current date and format it as yyyy-mm-dd
    const currentDate = new Date().toISOString().slice(0, 10);
    // get elements with ids "date1", "date2", and "fecha"
    var start_date = document.getElementById("date1")
    var end_date = document.getElementById("date2")
    var fecha = document.getElementById("fecha")
    // set the value of the date input fields to the current date
    start_date.value = currentDate
    end_date.value = currentDate
    // set the value of the fecha input field to the current date
    fecha.value = currentDate
}

function loadRegisters(){
    // Get the table element by its ID
    var table = document.getElementById("registros_table")
    // Clear the table contents
    table.innerHTML = ""
    // Add the table headers
    table.innerHTML += `<tr>
    <th style="width:170px">Fecha</th>
    <th style="width:240px">Producto</th>
    <th style="width:100px">Stock Inicial</th>
    <th style="width:70px">Entrada</th>
    <th style="width:70px">Salida</th>
    <th style="width:70px">Saldo</th>
    </tr>`
    // Loop through the reportes_list array and add a row for each reporte object
    for (i = 0; i < reportes_list.length; i++) {
        // If it's not the last row, add a row with normal border styling
        if(i != reportes_list.length-1){
            date = formatDate(reportes_list[i].hora)
            table.innerHTML += `<tr>
            <td>${date}</td>
            <td>${reportes_list[i].producto}</td>
            <td>${reportes_list[i].saldo_i}${reportes_list[i].unidad}</td>
            <td>${reportes_list[i].entrada}${reportes_list[i].unidad}</td>
            <td>${reportes_list[i].salida}${reportes_list[i].unidad}</td>
            <td>${reportes_list[i].saldo}${reportes_list[i].unidad}</td>
        </tr>`
        // If it's the last row, add a row with a bottom border of 0 to avoid double borders
        } else {
            date = formatDate(reportes_list[i].hora)
            table.innerHTML += `<tr>
            <td style="border-bottom: 0">${date}</td>
            <td style="border-bottom: 0">${reportes_list[i].producto}</td>
            <td style="border-bottom: 0">${reportes_list[i].saldo_i}${reportes_list[i].unidad}</td>
            <td style="border-bottom: 0">${reportes_list[i].entrada}${reportes_list[i].unidad}</td>
            <td style="border-bottom: 0">${reportes_list[i].salida}${reportes_list[i].unidad}</td>
            <td style="border-bottom: 0">${reportes_list[i].saldo}${reportes_list[i].unidad}</td>
        </tr>`
        }
    }
}

function formatDate(datetime){
    date = datetime.split(" ")[0]
    hour = datetime.split(" ")[1]
    date = date.split("-")
    hour = hour.split(":")
    return `${date[2]}/${date[1]}/${date[0]} ${hour[0]}:${hour[1]}`
}
