// Define a class called "Producto" with a constructor that takes four parameters: "id", "nombre", "unidad", and "stock".
class Producto{
    constructor(id, nombre, unidad, stock){
        this.id = id 
        this.nombre = nombre 
        this.unidad = unidad 
        this.stock = stock 
    }
}

// Define two empty arrays called "reportes_p_list" and "productos_list".
reportes_p_list = []
productos_list = []

// This function fetches the product registers from the database based on the product id
function fetchProductRegisters(producto) 
{
	// Fetch data from the server using a URL that includes the product id
	fetch(`../../db/fetchQuery.php?query=productRep&id=${producto.id}`).then(response => response.text()).then(data => {
        // Parse the JSON response data into a JavaScript object
        response = JSON.parse(data);
        // Create an empty list to store the report objects
        reportes_p_list = []
        // If there is data in the response, loop through each report object and create a new Reporte object
        if(response.length) {
            $.each(response, function(key,value) {
                reporte = new Reporte(value.IDREGISTRO, value.nombre, value.HORA, value.unidad, value.SALDOANT, value.ENTRADAPROD, value.SALIDAPROD, value.SALDOPROD)
                // Add the new Reporte object to the list of report objects
                reportes_p_list.push(reporte)
        	});
        }
        // Load the product registers using the product's unit of measurement
        loadProdcutRegisters(producto.unidad)
    });
}

// Function to fetch list of products from server
function fetchProductos() 
{
    // Fetching data using AJAX
    fetch(`../../db/fetchQuery.php?query=products`).then(response => response.text()).then(data => {
        // Parsing JSON data to JS object
        response = JSON.parse(data);
        productos_list = []
        // Looping through each product in the response
        if(response.length) {
            $.each(response, function(key,value) {
                // Creating a new instance of the Product class for each product
                producto = new Producto(value.id, value.nombre, value.unidad, value.cantidad)
                // Adding the new product to the list of products
                productos_list.push(producto)
        	});
        }
        // Calling the loadProductos() function to display the list of products
        loadProductos()
        // Fetching the list of product registers for the first product in the list
        fetchProductRegisters(productos_list[0])
        // Setting up the insert form for adding new product registers
        setInsertForm()
    });
}
// Function to load the table with product registers
function loadProdcutRegisters(unidad){
    // Get the table element
    var table = document.getElementById("registros_producto")
    // Clear the table contents
    table.innerHTML = ""
    // Add the table header
    table.innerHTML += `<tr>
    <th style="width:100px">Fecha</th>
    <th style="width:50px">Entrada</th>
    <th style="width:50px">Salida</th>
    <th style="width:50px">Saldo</th>
    </tr>`
    // Loop through the list of product registers and add each to the table
    for (i = 0; i < reportes_p_list.length; i++) {
        // Check if the current register is not the last one
        if(i != reportes_p_list.length-1){
            date = formatDate(reportes_p_list[i].hora).split(" ")[0]
            // Add a table row for the register
            table.innerHTML += `<tr>
            <td>${date}</td>
            <td>${reportes_p_list[i].entrada}${unidad}</td>
            <td>${reportes_p_list[i].salida}${unidad}</td>
            <td>${reportes_p_list[i].saldo}${unidad}</td>
        </tr>`
        } else {
            date = formatDate(reportes_p_list[i].hora).split(" ")[0]
            // Add a table row for the last register with a bottom border
            table.innerHTML += `<tr>
            <td style="border-bottom: 0">${date}</td>
            <td style="border-bottom: 0">${reportes_p_list[i].entrada}${unidad}</td>
            <td style="border-bottom: 0">${reportes_p_list[i].salida}${unidad}</td>
            <td style="border-bottom: 0">${reportes_p_list[i].saldo}${unidad}</td>
        </tr>`
        }
    }
}

function loadProductos(){
    // Get the select element by its ID
    var select = document.getElementById("productos")
    // Clear the select options
    select.innerHTML = ""
    // Loop through the productos_list array and add each producto as an option to the select element
    productos_list.forEach(producto =>{
        select.innerHTML += `<option value="${producto.id}">${producto.nombre}</option>`
    })
}

// This function sets the values for the insert form based on the selected product.
function setInsertForm(){
    var select = document.getElementById("productos")
    // Finds the selected product from the product list.
    producto =  productos_list.find(producto => producto.id == select.value)
    // Gets the current stock element and updates it with the current stock for the selected product.
    var cur_stock = document.getElementById("cur_stock")
    cur_stock.innerHTML = `Stock actual de ${producto.nombre}: ${producto.stock}${producto.unidad}`
    // Fetches the product registers for the selected product.
    fetchProductRegisters(producto)
    // Gets the plus and minus buttons and sets their onclick functions.
    var p_button = document.getElementById("p_button")
    var m_button = document.getElementById("m_button")
    p_button.onclick = function() {cantPlus(producto.unidad)}
    m_button.onclick = function() {cantMinus(producto.unidad)}
    // Sets the value of the quantity input field to zero with the corresponding unit for the selected product.
    var cantidad = document.getElementById("cantidad")
    cantidad.value = `0${producto.unidad}`
}

//Increases the quantity value by one and updates the input field
 
function cantPlus(unidad){
    var cantidad = document.getElementById("cantidad") // Get the input field for quantity
    cant = parseInt(cantidad.value.slice(0,-unidad.length)) // Remove the unit from the current value and parse it to an integer
    cant++ // Increase the quantity value by one
    cantidad.value = cant+unidad // Update the input field with the new value and add the unit back to the end
}

//Decreases the quantity value by one (if greater than zero) and updates the input field

function cantMinus(unidad){
    var cantidad = document.getElementById("cantidad") // Get the input field for quantity
    cant = parseInt(cantidad.value.slice(0,-unidad.length)) // Remove the unit from the current value and parse it to an integer
    if(cant > 0){ // Check if the current value is greater than zero
        cant-- // Decrease the quantity value by one
        cantidad.value = cant+unidad // Update the input field with the new value and add the unit back to the end
    }
}
function insertRegister(){
    // get selected product
    var select = document.getElementById("productos")
    producto =  productos_list.find(producto => producto.id == select.value)
    
    // get input values
    var cantidad = document.getElementById("cantidad")
    var fecha = document.getElementById("fecha")
    
    // create form data with input values
    var datos = new FormData();
    entrada = parseInt(cantidad.value.slice(0, -producto.unidad.length))
    datos.append("id", select.value);
    datos.append("entrada", entrada);
    datos.append("salida", 0);
    datos.append("fecha", fecha.value);
    
    // send POST request to server to insert register
    fetch("../../db/insertRegister.php", { method: "POST", body: datos })
        .then(response => response.text())
        .then(data => {
            // update product and register lists after successful insertion
            fetchProductos()
            fetchRegisters()
        });
}