class TicketR {
    constructor(id, movie, schedule, seat_n, hall, day, date, ci){
        this.id = id;
        this.movie = movie;
        this.schedule = schedule;
        this.seat_n = seat_n;
        this.hall = hall;
        this.day = day;
        this.date = date;
        this.ci = ci;
    }
}

class MovieR{
    constructor(title, video, audio){
        this.title = title
        this.video = video
        this.audio = audio
    }
}

tickect_r_list = []

function fetchTicketsR(ci){
    if(ci == undefined){
        ci = document.getElementById("ci_input").value
    }
    fetch(`../../db/fetchQuery.php?query=tickets&ci=${ci}`).then(response => response.text()).then(data => {
        response = JSON.parse(data);
        if(response.length) {
            tickect_r_list = []
            $.each(response, function(key,value) {
                m = new MovieR(value.NOMBRE, value.TIPO, value.AUD)
        		t = new TicketR(value.IDVENTA, m, value.HORARIO, value.ASIENTO, value.SALA, value.DIA, value.FECHA, value.CI)
                tickect_r_list.push(t)
                var name = document.getElementById("name")
                name.innerHTML = "Nombre: " + value.C_NOMBRE
        	});
            loadTicketsR()
            console.log(tickect_r_list)
        }
    });
    
}

function createTicket(ticket){
    return `<div class="ticket">
    <div class="ticket_row" style="justify-content: flex-start; gap: 10px;">
        <div class="title">${ticket.movie.title}</div>
        <div class="ticket_tag">${ticket.movie.video}</div>
        <div class="ticket_tag">${ticket.movie.audio}</div>
    </div>
    <div class="ticket_row">
        <div class="ticket_data">${ticket.schedule} - ${ticket.hall} - Butaca ${ticket.seat_n} - ${ticket.day}</div>
    </div>
    <div class="ticket_row">
        <div class="sell_data">Comprada el ${ticket.date} (CI: ${ticket.ci})</div>
        <div class="button" style="font-size: 14px;" onclick="printTicket(${ticket.id})">Imprimir</div>
    </div>
    </div>`
}

function loadTicketsR(){
    var list = document.getElementById("ticket_list")
    list.innerHTML = ""
    tickect_r_list.forEach(ticket => {
        new_node = document.createElement("div")
        new_node.innerHTML = createTicket(ticket)
        list.appendChild(new_node)
    });
}

function printAll(){
    tickect_r_list.forEach(ticket => {
        printTicket(ticket, ticket.hall, ticket.seat_n, ticket.schedule)
    });
}




