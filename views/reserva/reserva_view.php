<?php
    session_start();
    if(!isset($_SESSION["username"])){
        header("Location: ../unauthorized/unauthorized_view.html");
    }
    if(isset($_GET["ci"])){
        $ci =  $_GET["ci"];
    } else {
        $ci = "";
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reservas</title>
    <link rel="stylesheet" href="../page_styles.css">
    <link rel="stylesheet" href="reserva_styles.css">
</head>

<body onload="fetchTicketsR('<?php echo $ci ?>')">
    <div class="container">
        <div class="lat_menu">
            <div class="menu_opt" >
                <a href="../cartelera/cartelera_view.php" style="height:30px;"><img src="../../resource/svg/movie.svg" height="30px"></a> 
            </div>
            <div class="menu_opt">
                <a href="../snacks/snacks_view.php" style="height:30px;"><img src="../../resource/svg/food.svg" height="30px"></a>
            </div>
            <div class="menu_opt_selected">
                <a href="" style="height:30px;"><img src="../../resource/svg/register.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../cartelera_ad/cartelera_ad_view.php" style="height:30px;"><img src="../../resource/svg/admin.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <img src="../../resource/svg/log-out.svg" height="30px" onclick="logOut()">
            </div>
        </div>
        <div class="content" id="content">
            <h1>Boletos Reservados</h1>
            <div class="main_form">
                <div style="display: flex; gap: 20px; align-items: center;">
                    <div class="button" onclick="fetchTicketsR()">Buscar por C.I.</div>
                    <input type="text" class="ci_input" id="ci_input" value="<?php echo $ci ?>" placeholder="C.I.">
                    <div id="name">Nombre:</div>
                </div>
                <div class="button" onclick="printAll()">Imprimir todo</div>
            </div>
            <div class="ticket_list" id="ticket_list">
            </div>
        </div>
    </div>
    <div id="qr" style="display: none;"></div>
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="reserva_script.js"></script>
<script src="../cartelera/print_ticket_script.js"></script>
<script src="../login/login_script.js"></script>
</html>