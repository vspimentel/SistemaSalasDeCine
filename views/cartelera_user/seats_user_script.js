function loadSeats(hall, schedule, movie){
    content = document.getElementById("content")
    content.innerHTML = `
    <div style="display: flex; justify-content: space-between; align-items: center;">
        <h1 style="margin-bottom: 0;">Selección de butaca</h1>
    </div>
    <h1 style="margin-bottom: 0;">${movie.title} - ${movie.audio} - ${movie.video}</h1>
    <div style="display: flex; justify-content: space-between; align-items: center;">
        <h1 style="font-size: 28px">${hall.number} - ${schedule.time}</h1>
        <div style="display: flex; gap: 30px;">
                        <div class="input-wrapper">
                            <label for="video" class="label_select" style="left: 10px; top: -8px;">Dia</label>
                            <select name="video" class="input_select" id="input_day">
                                <option value="1">Lunes</option>
                                <option value="2">Martes</option>
                                <option value="3">Miércoles</option>
                                <option value="4">Jueves</option>
                                <option value="5">Viernes</option>
                                <option value="6">Sábado</option>
                                <option value="0">Domingo</option>
                            </select>
                        </div>
                        <div class="button_1" onclick="finishSelection()">Terminar selección</div>
                    </div>
    </div>
    <div id="table">
    </div>`
    date_input = document.getElementById("input_day")
    date_input.value = day
    date_input.onchange = function(){
        day = date_input.value
        fetchSeats(hall, schedule, day)
    }
    fetchSeats(seats_data[0], seats_data[1]);
}