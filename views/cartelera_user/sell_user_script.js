// This function opens the ticket selling overlay by setting its style display property to "flex"
function openForm() {
    overlay = document.getElementById("overlay")
    overlay.style = "display: flex;"
}

// This function closes the ticket selling overlay by setting its style display property to "none"
function closeForm() {
    overlay = document.getElementById("overlay")
    overlay.style = "display: none;"
}

// This function sells the tickets that have been selected by the user
function sellTickets() {
    // Retrieve the name and ID number entered by the user in the input fields
    var nombre = document.getElementById("input_nombre").value
    var ci = document.getElementById("input_ci").value
    // Iterate over the elements in the ticket_list array
    ticket_list.forEach(ticket => {
        var datos = new FormData();
        // Update the state of the seat associated with the ticket to "O" (occupied)
        updateSeatState(ticket.seat, "O", day)
        // Retrieve the price of the ticket from a price_dict object using the date and movie information associated with the ticket
        price = price_dict[day][ticket.movie.video]
        // Append several key-value pairs to the FormData object
        datos.append("pelicula", ticket.movie.id);
        datos.append("horario", ticket.schedule.time)
        datos.append("asiento", ticket.seat.seat_n)
        datos.append("precio", price)
        datos.append("sala", hall.number)
        datos.append("dia", day)
        datos.append("nombre", nombre)
        datos.append("ci", ci)
        // Send a POST request to a PHP script located in the "../../db/" directory named "insertMovieSellV.php", passing the FormData object as the request body
        fetch("../../db/insertMovieSellV.php", { method: "POST", body: datos })
            .then(response => response.text())
            .then(data => {console.log(data)});
    })
    ticket_list = []
    updateTicket()
    updatePrice()  
    location.href = "../reserva_user/reserva_user_view.php?ci=" + ci
    closeForm()
}

function updatePrice() {
    // Get a reference to the element that displays the total price
    const priceElement = document.getElementById("total_price");
    const priceForm = document.getElementById("payment_price");

    if (ticket_list.length > 0) {
        // Get the current date
        const today = new Date();

        // Get the price of the first ticket's movie and video type
        const movie = ticket_list[0].movie;
        const videoType = movie.video;
        const price = price_dict[day][videoType];

        // Calculate the total price of all tickets
        const totalPrice = ticket_list.length * price;

        // Display the total price with the appropriate formatting
        priceForm.innerHTML = `Total a pagar: ${totalPrice}Bs`
        priceElement.innerHTML = `${totalPrice}Bs`;
    } else {
        // If there are no tickets, display a price of 0
        priceForm.innerHTML = `Total a pagar: 0Bs`
        priceElement.innerHTML = `0Bs`;
    }
}

