// Ticket class to represent a movie ticket
class Ticket {
    constructor(id, movie, schedule, hall, seat) {
      this.id = id;
      this.movie = movie;
      this.schedule = schedule;
      this.hall = hall;
      this.seat = seat;
    }
  }
  
  // an array to store all the tickets, and a variable to keep track of the last ticket ID
  let ticket_list = [];
  let last_ticket_id = 1;

async function fetchLastSell() {
    // Use async/await syntax for readability and easier error handling
    const response = await fetch("../../db/fetchQuery.php?query=lastSell");
    
    // Use try/catch block to handle potential errors with fetch request
    try {
      const data = await response.json(); // Parse the response as JSON
      const [{ IDVENTA }] = data;
    
      // Update last_ticket_id with extracted IDVENTA
      last_ticket_id = IDVENTA;
    
    } catch (error) {
      // Handle any errors that occur during the fetch request
      console.error(error);
    }
  }

  function createTicket(ticket) {
    // Use template literals for readability and to simplify string concatenation
    return `
      <div class="ticket">
        <div class="ticket_title">${ticket.movie.title}</div>
        <div style="display: flex; justify-content: space-between;">
          <div class="ticket_tags">
            <div class="ticket_title_tag">
              ${ticket.movie.audio}
            </div>
            <div class="ticket_title_tag">
              ${ticket.movie.video}
            </div>
          </div>
          <div class="edit_buttons" onclick="deleteTicket(${ticket.id})">
            <img src="../../resource/svg/delete.svg" height="15px">
            Borrar
          </div>
        </div>
        <div style="display: flex; justify-content: space-between; align-items: center;">
          <div class="ticket_data">
            ${ticket.schedule.time} - ${ticket.hall.number} - Butaca ${ticket.seat.seat_n}
          </div>
        </div>
      </div>`;
  }

  function updateTicket() {
    const list = document.getElementById("ticket_list");
    const fragment = document.createDocumentFragment();
    
    ticket_list.forEach(ticket => {
      const new_node = document.createElement("div");
      new_node.innerHTML = createTicket(ticket);
      fragment.appendChild(new_node); // Append new_node to fragment
    });
    
    // Replace existing content with fragment's content in one DOM update
    list.textContent = "";
    list.appendChild(fragment);
  }


  function deleteTicket(ticket_id) {
    // Use destructuring to get the index and ticket object in one step
    const i = ticket_list.findIndex(({id}) => id === ticket_id);
    
    // Use filter to remove the selected seat object from selected_seats array
    selected_seats = selected_seats.filter(seat => seat.id !== ticket_list[i].seat.id);
    
    ticket_list.splice(i, 1);
    
    editSeats();
    updateTicket();
    updatePrice();
  }

  function clearTickets() {
    ticket_list.splice(0, ticket_list.length);
    selected_seats.splice(0, selected_seats.length);
    editSeats();
    updateTicket();
    updatePrice();
  }