// This code generates a ticket PDF file with a QR code containing ticket information
// It uses the QRCode.js library to generate the QR code and jsPDF to generate the PDF file

// Get the <div> element to place the QR code in
const div = document.getElementById("qr");

// Create a new QRCode object with the given options
const qrcode = new QRCode(div, {
    text: `ID:1`,
    width: 130,
    height: 130,
    colorDark: "#000000",
    colorLight: "#ffffff",
    correctLevel: QRCode.CorrectLevel.H,
});

// Define a function to load the logo image
async function loadLogoImage() {
    const response = await fetch("../../resource/images/logo.txt");
    const logoText = await response.text();
    return logoText;
}

// Define a function to print a ticket
async function printTicket(ticket, hall, seat, schedule) {
    // Load the logo image
    const logoPNG = await loadLogoImage();

    // Clear the QR code and generate a new one with the ticket ID
    qrcode.clear();
    qrcode.makeCode(`ID:${ticket.id}`);

    // Get the QR code as a Base64-encoded PNG image
    const QR = div.getElementsByTagName("canvas")[0].toDataURL("image/png");

    // Create a new jsPDF object with landscape orientation and A6 paper size
    const pdf = new jsPDF("l", "", "a6");

    // Set the font to Courier and bold
    pdf.getFontList();
    pdf.setFont("courier");
    pdf.setFontType("bold");

    // Add the logo image to the PDF file
    pdf.addImage(logoPNG, 15, 0);

    // Add the QR code to the PDF file
    pdf.addImage(QR, 82, 10);

    // Add the cinema name and ticket ID to the PDF file
    pdf.text("CINE SAS", 25, 54);
    pdf.text(`ENTRADA ${ticket.id}`, 80, 54);

    // Add the movie title, video and audio information to the PDF file
    pdf.text(ticket.movie.title, 15, 68);
    pdf.text(ticket.movie.video, 15, 78);
    pdf.text(`(${ticket.movie.audio})`, 23, 78);

    // Add the hall number to the PDF at (15, 88) position
    pdf.text(`SALA: ${hall.substr(5, 6)}`, 15, 88);

    // Add the schedule time to the PDF at (52, 88) position
    pdf.text(`HORA: ${schedule}`, 52, 88);

    // Add the seat number to the PDF at (45, 98) position
    pdf.text(`ASIENTO: ${seat}`, 15, 98);

    // Save the PDF file with a name "Entrada{id}.pdf"
    pdf.save(`Entrada${ticket.id}.pdf`);

}