<?php
    session_start();
    if(!isset($_SESSION["username"])){
        header("Location: ../unauthorized/unauthorized_view.html");
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cartelera</title>
    <link rel="stylesheet" href="cartelera_styles.css">
    <link rel="stylesheet" href="../page_styles.css">
    <link rel="stylesheet" href="../seat_select/seat_select_styles.css">
</head>
<body>
    <div class="container">
        <div class="lat_menu">
            <div class="menu_opt_selected" >
                <a href="#" style="height:30px;"><img src="../../resource/svg/movie.svg" height="30px"></a> 
            </div>
            <div class="menu_opt">
                <a href="../snacks/snacks_view.php" style="height:30px;"><img src="../../resource/svg/food.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../reserva/reserva_view.php" style="height:30px;"><img src="../../resource/svg/register.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../cartelera_ad/cartelera_ad_view.php" style="height:30px;"><img src="../../resource/svg/admin.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <img src="../../resource/svg/log-out.svg" height="30px" onclick="logOut()">
            </div>
        </div>
        <div class="content" id="content">
            <h1>Cartelera</h1>
            <div id="movies">
            </div>
        </div>
        <div class="ticket_list">
            <div style="display: flex; justify-content: space-between; align-items: center;">
                <h1>Boletos</h1>
                <div class="clean_button" onclick="clearTickets()" style="font-size: 18px;">Limpiar</div>
            </div>
            <div id="ticket_list""></div>
            <div class="input-wrapper">
                <label for="title" class="label_text_title">Nombre</label>
                <input type="text" name="title" class="input_text_title" id="nombre">
            </div>
            <div class="input-wrapper">
                <label for="title" class="label_text_title">C.I.</label>
                <input type="text" name="title" class="input_text_title" id="ci">
            </div>
            <div class="print_tickets" style="margin-top:20px">
                <div>Total: <span style="font-size: 25px; color: var(--Red);" id="total_price">0Bs</span></div>
                <div class="print_button" onclick="fetchSchedule(null, null, true)">Editar</div>
                <div class="print_button" onclick="sellTickets()">Imprimir</div>
            </div>
            <div id="qr" style="display: none;"></div>
        </div>
    </div>
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="movies_script.js"></script>
<script src="ticket_script.js"></script>
<script src="sell_script.js"></script>
<script src="print_ticket_script.js"></script>
<script src="print_bill_script.js"></script>
<script src="../seat_select/seats_script.js"></script>
<script src="../seat_select/hall_script.js"></script>
<script src="../login/login_script.js"></script>
<script>
    fetchMovies()
    fetchPrices()
    fetchLastSell()
</script>
</html>