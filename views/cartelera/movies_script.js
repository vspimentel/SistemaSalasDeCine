// Define objects for video and audio types
const Video = {
	DosD: '2D',
	TresD: '3D',
}

const Audio = {
	Subtitulada: 'Subtitulada',
	Doblada: 'Doblada',
}

// Define Schedule class with ID, time, and hall properties
class Schedule{
    constructor(id, time, hall){
        this.id = id
        this.time = time
        this.hall = hall
    }
}

// Define Movie class with ID, title, image, video, audio, and schedule properties
class Movie{
    constructor(id, title, image, video, audio, schedule){
        this.id = id
        this.title = title
        this.image = image
        this.video = video
        this.audio = audio
        this.schedule = schedule
    }
}

// Create an empty array to hold the list of Movie objects
movie_list = []

// Generate HTML code for displaying a Movie object
function createMovie(movie){
    return `<div class="movie_data">
    <div class="movie_pic">
        <div style="background-image: url(../../resource/images/${movie.image});" class="movie">
            <div class="movie_tag">${movie.video}</div>
            <div class="movie_tag">${movie.audio.substr(0,3).toUpperCase()}</div>
        </div>
    </div>
    <div class="titles">${movie.title}</div>
    <div id="schedule_${movie.id}"></div>
</div>`
}

// Generate HTML code for displaying a Schedule object
function createTimeTags(schedule, movie){
    return `<div class="time_tag" onclick="fetchSchedule(${schedule.id}, ${movie}, false)">
    <div id="hour">${schedule.time}</div>
</div>`
}

async function fetchMovies() {
    try {
        // Fetch the movie data from the API using a relative URL
        const response = await fetch("../../db/fetchQuery.php?query=movies");

        // Parse the response data as JSON
        const data = await response.json();

        // If there are movies in the response, process them
        if (data.length) {
            // Create an empty array to store the movie objects
            movie_list = [];

            // Loop through each movie in the response data
            data.forEach((value) => {
                // Check if the movie is already in the movieList array
                const movie = movie_list.find((m) => m.id === value.IDPELI);

                if (movie) {
                    // If the movie is already in the array, add the schedule to its schedule array
                    movie.schedule.push(new Schedule(value.IDFUNCION, value.HORARIO, value.NOMBRE_SALA));
                } else {
                    // If the movie is not in the array, create a new Movie object and add it to the array
                    const newMovie = new Movie(value.IDPELI, value.NOMBRE, value.POSTER, value.TIPO, value.AUD, []);
                    newMovie.schedule.push(new Schedule(value.IDFUNCION, value.HORARIO, value.NOMBRE_SALA));
                    movie_list.push(newMovie);
                }
            });

            // Once all movies have been added to the movieList array, display them on the web page
            loadMovies();
        }
    } catch (error) {
        // If there's an error fetching the movie data, log an error message with the error details
        console.error("Error fetching movies:", error);
    }
}

// Generate HTML code for displaying all the movies and their schedules on the web page
function loadMovies(){
    var list = document.getElementById("movies")
    list.innerHTML = ""
    var i = 0
    movie_list.forEach(movie => {
        // if the counter is a multiple of 3, create a new row
        if(i%3 == 0){
            row = document.createElement("div")
            row.className = "movie_row"
            list.appendChild(row)
        }
        // create a new node to hold the HTML for the current movie
        new_node = document.createElement("div")
        new_node.innerHTML = createMovie(movie)
        row.appendChild(new_node)
        loadSchedule(movie.id, movie.schedule)
        i++
    });
}

// function to load the schedule for a movie
function loadSchedule(movie_id, schedule_list){
    var list = document.getElementById("schedule_" + movie_id)
    list.innerHTML = ""
    var i = 0
    schedule_list.forEach(s => {
        // if the counter is a multiple of 3, create a new row
        if(i%3 == 0){
            s_row = document.createElement("div")
            s_row.className = "schedule_row"
            list.appendChild(s_row)
        }
        // create a new node to hold the HTML for the current schedule item
        new_node = document.createElement("div")
        new_node.innerHTML = createTimeTags(s, movie_id)
        s_row.appendChild(new_node)
        i++
    });
}
