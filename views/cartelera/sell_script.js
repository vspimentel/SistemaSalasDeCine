// Declare an empty dictionary to store prices
const price_dict = {}

// Fetch prices from the database
async function fetchPrices(){
    try {
        // Make a request to fetch prices using fetch API and wait for the response
        const response = await fetch("../../db/fetchQuery.php?query=price")
        // Parse the response data as JSON and wait for the result
        const data = await response.json();
        // Loop through all prices in the response
        for (const value of data) {
            // Add a new key-value pair to the dictionary for each price
            price_dict[value.IDDIA] = {}
            price_dict[value.IDDIA]["2D"] = value.PRECIO
            price_dict[value.IDDIA]["3D"] = value.PRECIO3D
        }
    } catch (error) {
        // Log any errors that occur
        console.error(error)
    }
}

function updatePrice() {
    // Get a reference to the element that displays the total price
    const priceElement = document.getElementById("total_price");

    if (ticket_list.length > 0) {
        // Get the current date
        const today = new Date();

        // Get the price of the first ticket's movie and video type
        const movie = ticket_list[0].movie;
        const videoType = movie.video;
        const price = price_dict[day][videoType];

        // Calculate the total price of all tickets
        const totalPrice = ticket_list.length * price;

        // Display the total price with the appropriate formatting
        priceElement.innerHTML = `${totalPrice}Bs`;
    } else {
        // If there are no tickets, display a price of 0
        priceElement.innerHTML = `0Bs`;
    }
}

function sellTickets() {
    nombre = document.getElementById("nombre").value;
    ci = document.getElementById("ci").value;
    if(ci != "" && nombre != ""){
        printBill(ticket_list, nombre, ci)
    }
    // Loop through each ticket in the list of selected tickets
    for (const ticket of ticket_list) {
        // Print the ticket information to the screen
        printTicket(ticket, ticket.hall.number, ticket.seat.seat_n, ticket.schedule.time);
        // Update the seat state to "O" to mark it as sold
        updateSeatState(ticket.seat, "O", day);

        // Get the price of the ticket's movie and video type
        const movie = ticket.movie;
        const videoType = movie.video;
        const price = price_dict[day][videoType];

        // Create a new FormData object to send the ticket information to the server
        const formData = new FormData();
        formData.append("pelicula", ticket.movie.id);
        formData.append("horario", ticket.schedule.time);
        formData.append("asiento", ticket.seat.seat_n);
        formData.append("precio", price);
        formData.append("sala", hall.number);
        formData.append("dia", day);

        // Send the ticket information to the server using fetch API
        fetch("../../db/insertMovieSell.php", { method: "POST", body: formData })
            .then(response => response.text())
            .then(data => {})
            .catch(error => {
                // Log any errors that occur during the fetch request
                console.error(error);
            });
    }

    // Clear the list of selected seats and tickets
    selected_seats = [];
    ticket_list = [];

    // Update the ticket and price displays
    updateTicket();
    updatePrice();

    // Edit the seat colors to reflect the updated state
    editSeats();

    document.getElementById("nombre").value = "";
    document.getElementById("ci").value = "";
}