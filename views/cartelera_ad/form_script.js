last_schedule_id = 1
schedule_edit_list = []

// Adds 1 minute to the time displayed in the input field with id "time"
function timePlus(){
    var time = document.getElementById("time");
    
    // Create a new Date object with the current time
    var currentTime = new Date();
    
    // Update the hours and minutes of the current time with the values from the input field
    currentTime.setHours(parseInt(time.value.substr(0,2)));
    currentTime.setMinutes(parseInt(time.value.substr(3,2)));
    
    // Set the seconds and milliseconds of the current time to 0
    currentTime.setSeconds(0);
    currentTime.setMilliseconds(0);
    
    // Create a new Date object with the time 1 minute after the current time
    var nextTime = new Date(currentTime.getTime() + 60*1000);
    
    // Format the new time as a string in the format "HH:MM"
    var formattedTime = formatTime(nextTime);
    
    // Update the value of the input field with the new time
    time.value = formattedTime;
}

// Subtracts 1 minute from the time displayed in the input field with id "time"
function timeMinus(){
    var time = document.getElementById("time");
    
    // Create a new Date object with the current time
    var currentTime = new Date();
    
    // Update the hours and minutes of the current time with the values from the input field
    currentTime.setHours(parseInt(time.value.substr(0,2)));
    currentTime.setMinutes(parseInt(time.value.substr(3,2)));
    
    // Set the seconds and milliseconds of the current time to 0
    currentTime.setSeconds(0);
    currentTime.setMilliseconds(0);
    
    // Create a new Date object with the time 1 minute before the current time
    var prevTime = new Date(currentTime.getTime() - 60*1000);
    
    // Format the new time as a string in the format "HH:MM"
    var formattedTime = formatTime(prevTime);
    
    // Update the value of the input field with the new time
    time.value = formattedTime;
}

// Formats a Date object as a string in the format "HH:MM"
function formatTime(date) {
    var hours = date.getHours().toString().padStart(2, '0');
    var minutes = date.getMinutes().toString().padStart(2, '0');
    return `${hours}:${minutes}`;
}
function createSchedule(schedule){
    return `<div class="schedule">
    ${schedule.time} | ${schedule.hall.substr(0,1)}${schedule.hall.substr(5,6)}
    <img src="../../resource/svg/close.svg" height="17px" onclick="deleteSchedule(${schedule.id})">
</div>`
}

function updateScheduleTable() {
    var scheduleTable = document.getElementById('schedule_table');
    scheduleTable.innerHTML = '';
  
    var fragment = document.createDocumentFragment(); // create a document fragment
  
    var i = 0;
    schedule_edit_list.forEach(s => {
      if (i % 2 === 0) {
        row = document.createElement('div');
        row.className = 'schedule_table_row';
        fragment.appendChild(row); // append to the fragment instead of the table
      }
      var new_node = document.createElement('div');
      new_node.innerHTML = createSchedule(s);
      row.appendChild(new_node);
      i++;
    });
  
    scheduleTable.appendChild(fragment); // append the fragment to the table
  }

  function addSchedule() {
    var timeInput = document.getElementById("time");
    var hallSelect = document.getElementById("hall_select");
    var time = timeInput.value.trim();
    var hall = hallSelect.value;

    // Create a new schedule and add it to the edit list
    var schedule = new Schedule(last_schedule_id, time, hall);
    last_schedule_id++;
    schedule_edit_list.push(schedule);

    // Update the schedule table
    updateScheduleTable();
}

function deleteSchedule(scheduleId) {
    const index = schedule_edit_list.findIndex(schedule => schedule.id === scheduleId);
    if (index !== -1) {
      schedule_edit_list.splice(index, 1);
      updateScheduleTable();
    } else {
      console.error(`Schedule with ID ${scheduleId} not found.`);
    }
  }


function loadImg() {
    var previewElement = document.getElementById("preview");
    var selectedFile = document.getElementById("imagenIn").files[0];
    
    if (selectedFile) {
        previewElement.style.backgroundImage = `url(${URL.createObjectURL(selectedFile)})`;
    }
}

function loadMovieEdit(movieId) {
    // Find the selected movie from movie_list
    const movie = movie_list.find(movie => movie.id == movieId);

    // Update the UI elements with the movie details
    const titulo = document.getElementById("title_edit");
    const id = document.getElementById("id");
    const preview = document.getElementById("preview");
    const oldImage = document.getElementById("oldimg");
    const inTitle = document.getElementById("input_title");
    const inVideo = document.getElementById("input_video");
    const inAudio = document.getElementById("input_audio");
    const button = document.getElementById("execute_button");

    titulo.innerHTML = movie.title;
    preview.style.backgroundImage = `url(../../resource/images/${movie.image})`;
    oldImage.value = movie.image;
    id.value = movieId;
    inTitle.value = movie.title;
    inVideo.value = movie.video;

    // Set the selected audio option
    inAudio.value = movie.audio;
 
    // Set the schedule_edit_list to the movie's schedule
    schedule_edit_list = movie.schedule;

    // Update the button text and event listener
    button.innerHTML = "Editar película";
    button.onclick = editMovie;

    // Update the schedule table
    updateScheduleTable();
}
function cleanForm(){
    // Get elements from the DOM
    var titulo = document.getElementById("title_edit")
    var preview = document.getElementById("preview")
    var inTitle = document.getElementById("input_title")
    var inVideo = document.getElementById("input_video")
    var inAudio = document.getElementById("input_audio")
    var button = document.getElementById("execute_button")
    var archivo = document.getElementById("imagenIn").files;
    
    // Reset the values of the elements
    titulo.innerHTML = "Insertando película"
    preview.style.backgroundImage = `none`
    inTitle.value = ""
    inVideo.value = "2D"
    inAudio.value = "SUB"
    schedule_edit_list = []
    button.innerHTML = "Registrar película"
    
    // Clear the selected image file if any
    if(archivo){
        document.getElementById("imagenIn").value = ""
    }
    
    // Set the button onclick event handler to insertMovie
    button.onclick = insertMovie
    
    // Update the schedule table
    updateScheduleTable()
}