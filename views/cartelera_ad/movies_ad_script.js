function createMovieAd(movie){
    return `<div class="movie_data">
    <div class="movie_pic">
        <div style="background-image: url(../../resource/images/${movie.image});" class="movie_ad">
            <div style="display: flex; justify-content: flex-end; gap: 5px;">
                <div class="movie_tag">${movie.video}</div>
                <div class="movie_tag">${movie.audio.substr(0,3).toUpperCase()}</div>
            </div>
            <div style="display: flex; justify-content: space-between;">
                <div class="edit_buttons" onclick="deleteMovieFromView(${movie.id})">
                    <img src="../../resource/svg/delete.svg" height="15px">
                    Borrar
                </div>
                <div class="edit_buttons" onclick="loadMovieEdit(${movie.id})">
                    <img src="../../resource/svg/edit.svg" height="15px">
                    Editar
                </div>
            </div>
        </div>
    </div>
    <div class="titles">${movie.title}</div>
    <div id="schedule_${movie.id}"></div>
</div>`
}

// This function displays all the movies in the movie_list array
function loadMovies(){
    var list = document.getElementById("movies")
    list.innerHTML = "" // Clear the list to avoid duplicates
    var i = 0
    movie_list.forEach(movie => {
        if(i%3 == 0){
            row = document.createElement("div")
            row.className = "movie_row"
            list.appendChild(row)
        }
        new_node = document.createElement("div")
        new_node.innerHTML = createMovieAd(movie) // Add the movie advertisement HTML to the list
        row.appendChild(new_node)
        loadSchedule(movie.id, movie.schedule) // Load the schedule for the current movie
        i++
    });
}






