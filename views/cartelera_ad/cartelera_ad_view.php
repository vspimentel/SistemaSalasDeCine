<?php
    session_start();
    if(!isset($_SESSION["username"]) || $_SESSION["isadmin"] == 0){
        header("Location: ../unauthorized/unauthorized_view.html");
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrar cartelera</title>
    <link rel="stylesheet" href="cartelera_ad_styles.css">
    <link rel="stylesheet" href="../page_styles.css">
</head>

<body>
    <div class="container">
        <div class="lat_menu">
            <div class="menu_opt_selected">
                <a href="#" style="height:30px;"><img src="../../resource/svg/movie.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../snacks_ad/snacks_ad_view.php" style="height:30px;"><img src="../../resource/svg/food.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../reporte/reporte_view.php" style="height:30px;"><img src="../../resource/svg/stats.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../user_list/user_list_view.php" style="height:30px;"><img src="../../resource/svg/user_list.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../cartelera/cartelera_view.php" style="height:30px;"><img src="../../resource/svg/user1.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <img src="../../resource/svg/log-out.svg" height="30px" onclick="logOut()">
            </div>
        </div>
        <div class="content">
            <h1>Cartelera</h1>
            <div id="movies">
            </div>
        </div>
        <div class="edit_form">
            <div style="display: flex; justify-content: space-between; align-items:center; width: 100%;">
                <h1 style="margin-bottom: 0;">Editar</h1>
                <div class="button" style="padding: 7px;" onclick="cleanForm()">
                    <img src="../../resource/svg/new.svg" height="15px">
                    Insertar nueva pelicula
                </div>
            </div>
            <h1 style="margin-bottom: 0; font-size: 25px; margin-bottom: 10px; width: 100%;" id="title_edit">Insertando película</h1>
            <div class="movie_pic" style="border: 1px solid var(--DarkBlue);">
                <div class="movie_ad" id="preview"></div>
            </div>
            <label for="imagenIn" class="label_file">
                <img src="../../resource/svg/image.svg" height="18px">
                Insertar imagen
                <input id="imagenIn" type="file" style="display: none;" onchange="loadImg()" />
            </label>
            <input type="hidden" id="oldimg">
            <input type="hidden" id="id">
            <div class="input-wrapper">
                <label for="title" class="label_text_title">Título</label>
                <input type="text" name="title" class="input_text_title" id="input_title">
            </div>
            <div style="display: flex;">
                <div class="input-wrapper">
                    <label for="video" class="label_select" style="left: 20px;">Video</label>
                    <select name="video" class="input_select" style="height: 40px; width: 75px;" id="input_video">
                        <option value="2D">2D</option>
                        <option value="3D">3D</option>
                    </select>
                </div>
                <div class="input-wrapper">
                    <label for="audio" class="label_select" style="left: 20px;">Audio</label>
                    <select name="audio" class="input_select" style="height: 40px; width: 155px;" id="input_audio">
                        <option value="SUB">Subtitulada</option>
                        <option value="DOB">Doblada</option>
                    </select>
                </div>
            </div>
            <div style="display: flex;">
                <div class="input-wrapper">
                    <label for="time" class="label_text_time">Horario</label>
                    <input type="text" name="time" class="input_text_time" value="01:00" readonly="true" id="time">
                </div>
                <div style="display: flex; flex-direction: column;">
                    <div class="pm_button" style="margin-top: 10px;" id="p_button" onclick="timePlus()">+</div>
                    <div class="pm_button" style="margin-top: 5px;" id="m_button" onclick="timeMinus()">-</div>
                </div>
                <div class="input-wrapper">
                    <label for="hall" class="label_select" style="left: 20px; font-size: 17px;">Sala</label>
                    <select name="hall" class="input_select" style="height: 60px; width: 100px; font-size: 20px;"
                        id="hall_select">
                        <option value="Sala 1">Sala 1</option>
                        <option value="Sala 2">Sala 2</option>
                        <option value="Sala 3">Sala 3</option>
                    </select>
                </div>
            </div>
            <div class="button" style="width: 200px; height: 40px; font-size: 18px" onclick="addSchedule()">
                Agregar horario <span style="font-size: 30px; line-height: 0px;">+</span>
            </div>
            <div class="schedule_table" id="schedule_table"></div>
            <div style="display: flex; justify-content: flex-end; width: 250px;">
                <div class="button" onclick="insertMovie()" id="execute_button">Registrar pelicula</div>
            </div>
        </div>
    </div>
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="../cartelera/movies_script.js"></script>
<script src="movies_ad_script.js"></script>
<script src="movies_bd_script.js"></script>
<script src="form_script.js"></script>
<script src="../login/login_script.js"></script>
<script>
    fetchMovies()
</script>
</html>