// This function deletes a movie from the movie_list array and the database,
// and then reloads the movie list.
function deleteMovieFromView(movie_id) {
    // Find the index of the movie with the given id in the movie_list array.
    i = movie_list.findIndex(movie => movie.id == movie_id)
    // Ask for confirmation to delete the movie, showing its title.
    if (confirm(`Desea eliminar la película:\n${movie_list[i].title}`)) {
        // If the user confirms, remove the movie from the movie_list array.
        movie_list.splice(i, 1)
        // Delete the movie from the database by calling the deleteMovie function.
        deleteMovie(movie_id)
        // Reload the movie list by calling the loadMovies function.
        loadMovies()
    }
}

// This function deletes a movie from the database by making a POST request
// to the deleteMovie.php script.
function deleteMovie(movie_id) {
    // Create a new FormData object and append the movie_id to it.
    var datos = new FormData();
    datos.append("id", movie_id);
    // Make a POST request to the deleteMovie.php script using the fetch API.
    fetch("../../db/deleteMovie.php", { method: "POST", body: datos })
        // Convert the response to text.
        .then(response => response.text())
        // Clean the form by calling the cleanForm function.
        .then(data => {
            cleanForm()
        });
}

// This function updates a movie in the database by making a POST request
// to the editMovie.php script.
function editMovie() {
    // Get the necessary input elements from the HTML document.
    var image = document.getElementById("imagenIn")
    var oldImage = document.getElementById("oldimg")
    var id = document.getElementById("id")
    var inTitle = document.getElementById("input_title")
    var inVideo = document.getElementById("input_video")
    var inAudio = document.getElementById("input_audio")
    var schedule = ""
    // Build the schedule string by concatenating the time and hall values of
    // each element in the schedule_edit_list array.
    for (var i = 0; i < schedule_edit_list.length; i++) {
        schedule += `${schedule_edit_list[i].time}-${schedule_edit_list[i].hall}`
        if (i < schedule_edit_list.length - 1) {
            schedule += "|"
        }
    }
    // Create a new FormData object and append the necessary data to it.
    var datos = new FormData();
    datos.append("id", id.value);
    datos.append("posterOld", oldImage.value);
    datos.append("nombre", inTitle.value);
    datos.append("video", inVideo.value);
    datos.append("audio", inAudio.value);
    datos.append("horario", schedule);
    datos.append("poster", image.files[0]);
    // Make a POST request to the editMovie.php script using the fetch API.
    fetch("../../db/editMovie.php", { method: "POST", body: datos })
        // Convert the response to text.
        .then(response => response.text())
        // Clean the form by calling the cleanForm function.
        // Reload the movie list by calling the fetchMovies function.
        .then(data => {
            cleanForm()
            fetchMovies()
        });
}

// This function inserts a new movie into the database by making a POST
// request to the insertMovie.php script.
function insertMovie() {
    // Get the necessary input elements from the HTML document.
    var image = document.getElementById("imagenIn")
    var inTitle = document.getElementById("input_title")
    var inVideo = document.getElementById("input_video")
    var inAudio = document.getElementById("input_audio")
    var schedule = ""
    // Build the schedule string by concatenating the time and hall values of
    // each element in the schedule_edit_list array.
    for (var i = 0; i < schedule_edit_list.length; i++) {
        schedule += `${schedule_edit_list[i].time}-${schedule_edit_list[i].hall}`
        if (i < schedule_edit_list.length - 1) {
            schedule += "|"
        }
    }
    // Create a new FormData object and append the necessary data to it.
    var datos = new FormData();
    datos.append("nombre", inTitle.value);
    datos.append("video", inVideo.value);
    datos.append("audio", inAudio.value);
    datos.append("horario", schedule);
    datos.append("poster", image.files[0]);
    // Make a POST request to the insertMovie.php script using the fetch API.
    fetch("../../db/insertMovie.php", { method: "POST", body: datos })
        // Convert the response to text.
        .then(response => response.text())
        // Clean the form by calling the cleanForm function.
        // Reload the movie list by calling the fetchMovies function.
        .then(data => {
            cleanForm()
            fetchMovies()
        });
}