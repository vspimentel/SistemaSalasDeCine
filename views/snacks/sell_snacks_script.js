
// This function updates the total price displayed on the HTML page based on the current list of bill entries.
function updatePrice() {
    // Get the element where the total price should be displayed.
    var price_d = document.getElementById("total_price")

    // Initialize the total price to zero.
    var price = 0

    // Loop through each bill entry and add its cost to the total price.
    bill_entry_list.forEach(bill_entry => {
        price += bill_entry.cantidad * bill_entry.snack.precio
    })

    // Update the HTML content of the price element to display the new total price.
    price_d.innerHTML = `${price}Bs`
}

// This function takes an array of arrays, where each inner array contains two elements, and returns a new array
// that sums the second element of each inner array for each unique value of the first element.
function sumByFirstValue(arr) {
    const result = [];          // Initialize the result array.
    const firstValues = [];     // Initialize an array to keep track of the unique first values seen so far.

    // Loop through each inner array in the input array.
    for (let i = 0; i < arr.length; i++) {
        const currentFirstValue = arr[i][0];    // Get the current first value.
        const currentSecondValue = arr[i][1];   // Get the current second value.

        // If the current first value has not been seen before, add a new entry to the result array that
        // includes the current first and second values, and add the current first value to the list of
        // unique first values seen so far.
        if (!firstValues.includes(currentFirstValue)) {
            result.push([currentFirstValue, currentSecondValue]);
            firstValues.push(currentFirstValue);
        }
        // If the current first value has been seen before, find the existing entry in the result array
        // that corresponds to that first value, and add the current second value to its second element.
        else {
            for (let j = 0; j < result.length; j++) {
                if (result[j][0] === currentFirstValue) {
                    result[j][1] += currentSecondValue;
                    break;
                }
            }
        }
    }
    return result;     // Return the final result array.
}

// This function is called when the user clicks the "Sell" button in the interface. It updates the database
// with information about the snacks that were sold, and also updates the inventory based on the amount of
// each snack that was sold.
function sellSnacks() {
    const nombre = document.getElementById("nombre").value 
    const ci = document.getElementById("ci").value   
    if(ci != "" && nombre != ""){
        printBill(bill_entry_list, nombre, ci)     // Call the printBill() function to print the bill.
    } 
    consumo_total = []      // Initialize an empty array to hold the total consumption of each snack.

    // Loop through each bill entry in the bill entry list.
    bill_entry_list.forEach(bill_entry => {
        insertVenta(bill_entry);     // Call the insertVenta() function to insert the sale into the database.

        // Loop through each item in the consumo object of the current bill entry, and add it to the consumo_total array.
        for (key in bill_entry.consumo) {
            consumo_total.push(bill_entry.consumo[key])
        }
    })

    // Call the sumByFirstValue() function to group the consumption data by snack ID and sum the quantities sold.
    consumo_total = sumByFirstValue(consumo_total)

    // Loop through each item in the consumo_total array, and call the storageUpdate() function to update the inventory.
    consumo_total.forEach(consumo => {
        storageUpdate(consumo)
    })
}

// This function is called when a snack is sold, and it inserts a new record into the database to reflect the sale.
function insertVenta(bill_entry) {
    // Create a new FormData object to hold the data that will be sent to the server.
    var datos = new FormData();
    datos.append("id", bill_entry.id);      // Add the ID of the snack to the FormData object.
    datos.append("cantidad", bill_entry.cantidad);   // Add the quantity sold to the FormData object.
    datos.append("precio", bill_entry.cantidad * bill_entry.snack.precio)   // Calculate the total price of the sale and add it to the FormData object.

    // Use the fetch() method to send the FormData object to the server.
    fetch("../../db/insertSnackSell.php", { method: "POST", body: datos })
        .then(response => response.text())   // When the server responds, convert the response to text.
        .then(data => {
            bill_entry_list = []   // Clear the bill entry list.
            updateProductList()    // Call the updateProductList() function to update the list of available snacks.
            updatePrice()          // Call the updatePrice() function to update the total price of the bill.
            loadSnacks()           // Call the loadSnacks() function to reload the list of snacks that have been sold.
        });
}

// This function updates the inventory levels in the database after a snack is sold.
// The function takes in a consumption array containing the ID of the snack and the amount sold.
function storageUpdate(consumption) {
    var datos = new FormData();
    console.log(consumption)
    datos.append("id", consumption[0]);   // Add the ID of the snack to the form data.
    datos.append("salida", consumption[1]);   // Add the amount sold to the form data.
    datos.append("entrada", 0);   // Set the amount added to 0, as the snack is being sold, not restocked.
    // Send a POST request to the storageUpgrade.php script with the form data.
    fetch("../../db/storageUpgrade.php", { method: "POST", body: datos })
        .then(response => response.text())   // Once the response is received, convert it to text.
        .then(data => { });   // Do nothing with the response, as it is not needed for this function.
}