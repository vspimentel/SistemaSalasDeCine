function printBill(bill_entry_list, nombre, ci) {
    const today = new Date();
    const date = formatDate(today)
    const hour = formatHour(today)
    var last_y = 66
    var price = 0
    bill_entry_list.forEach(bill_entry => {
        price += bill_entry.cantidad * bill_entry.snack.precio
    })
    const pdf = new jsPDF('p', '', 'b6');
    pdf.setFont("courier");
    pdf.setFontSize(9);
    pdf.text('CINE SAS', 54, 10);
    pdf.text('FACTURA', 55, 14);
    pdf.text('(Con Derecho a Credito Fiscal)', 35, 18);
    pdf.text('-----------------------------------------', 25, 22);
    pdf.text('NIT', 50, 26);
    pdf.text(' : ', 55, 26);
    pdf.text('1000465024', 60, 26);
    pdf.text('No. FACTURA', 35, 30);
    pdf.text(' : ', 55, 30);
    pdf.text('000174097', 60, 30);
    pdf.text('COD. AUTORIZACION', 24, 34);
    pdf.text(' : ', 55, 34);
    pdf.text('392095883858588540058', 60, 34);
    pdf.text('-----------------------------------------', 25, 38);
    pdf.text('Fecha', 27, 42);
    pdf.text(' : ', 36, 42);
    pdf.text(date, 42, 42);
    pdf.text('Hora', 77, 42);
    pdf.text(' : ', 86, 42);
    pdf.text(hour, 92, 42);
    pdf.text('Nombre', 27, 46);
    pdf.text(' : ', 36, 46);
    pdf.text(nombre, 42, 46);
    pdf.text('NIT/CI', 27, 50);
    pdf.text(' : ', 36, 50);
    pdf.text(ci, 42, 50);
    pdf.text('Cliente', 27, 54);
    pdf.text(' : ', 38, 54);
    pdf.text('129387475', 42, 54);
    pdf.text('=========================================', 25, 58);
    pdf.text('Cantidad U/M', 30, 62);
    pdf.text('Precio/U', 66, 62);
    pdf.text('Monto', 94, 62);
    pdf.text('=========================================', 25, 66);
    bill_entry_list.forEach(function (bill_entry) {
        pdf.text(bill_entry.snack.nombre, 25, last_y + 4);
        pdf.text(`${parseFloat(bill_entry.cantidad).toFixed(2)} UND`, 34, last_y + 8);
        pdf.text(parseFloat(bill_entry.snack.precio).toFixed(2), 74, last_y + 8);
        pdf.text(parseFloat(bill_entry.snack.precio*bill_entry.cantidad).toFixed(2), 94, last_y + 8);
        last_y += 8;
    });
    pdf.text('==============', 78, last_y + 4);
    pdf.text('Subtotal:', 46, last_y + 8);
    pdf.text(parseFloat(price).toFixed(2), 94, last_y + 8);
    pdf.text('Descuento:', 46, last_y + 12);
    pdf.text('0.00', 94, last_y + 12);
    pdf.text('==============', 78, last_y + 16);
    pdf.text('Monto a Pagar: BS', 36, last_y + 20);
    pdf.text(parseFloat(price).toFixed(2), 94, last_y + 20);
    pdf.text('-----------------------------------------', 25, last_y + 24);
    pdf.text('GRACIAS POR SU PREFERENCIA !!!!!!', 35, last_y + 28);
    pdf.text('-----------------------------------------', 25, last_y + 32);
    pdf.save('Factura.pdf');
}

function formatDate(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    date = dd + '/' + mm + '/' + yyyy;
    return date;
}

function formatHour(date) {
    var hh = date.getHours();
    var mm = date.getMinutes();
    if (hh < 10) {
        hh = '0' + hh
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    date = hh + ':' + mm;
    return date;
}