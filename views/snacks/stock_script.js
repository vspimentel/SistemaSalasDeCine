// Define the Producto class with a constructor that takes tipo, stock, unidad, and optList as parameters
class Producto{
    constructor(tipo, stock, unidad, optList){
        this.tipo = tipo
        this.stock = stock
        this.unidad = unidad
        this.optList = optList
    }
}

// Create an empty list to store the available products
product_stock = []

// Define the createProduct function that takes a product object as a parameter and returns a string of HTML code
// representing the product and its stock
function createProduct(product){
    // Convert the stock value to a string with one decimal place if the unit of measure is not " "
    stock = product.stock.toFixed(1)
    if(product.unidad == " "){
        // Convert the stock value to a string with no decimal places if the unit of measure is " "
        stock = product.stock.toFixed(0)
    }
    // Return the HTML code for the product and its stock
    return `<div style="font-size: 12px;">${product.tipo}: ${stock}${product.unidad}</div>`
}

// Define the loadProducts function that takes a snack_id and a list of products as parameters
// and adds HTML elements representing the products to the DOM
function loadProducts(snack_id, product_list){
    // Get the DOM element for the list of products with the given snack_id
    var list = document.getElementById("products_" + snack_id)
    // Clear the contents of the list element
    list.innerHTML = ""
    // Iterate over each product in the list of products
    product_list.forEach(producto => {
        // Create a new div element
        new_node = document.createElement("div")
        // Set the innerHTML of the new div element to the HTML code for the product and its stock
        new_node.innerHTML = createProduct(producto)
        // Append the new div element to the list element
        list.appendChild(new_node)
    });
}

//Calculates the minimum stock among a list of products based on their quantities in a given combo.
function getStock(product_list, cant_list){
    // Create an empty array to store the stocks
    stocks = []
    // Iterate over the product list
    product_list.forEach(product => {
        // Calculate the stock per unit based on the combo quantity and add it to the array
        stocks.push(product.stock/cant_list[product.tipo])
    });
    // Return the integer value of the minimum stock
    return Math.trunc(Math.min(...stocks))
}
