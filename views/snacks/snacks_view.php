<?php
    session_start();
    if(!isset($_SESSION["username"])){
        header("Location: ../unauthorized/unauthorized_view.html");
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Venta de snacks</title>
    <link rel="stylesheet" href="snacks_styles.css">
    <link rel="stylesheet" href="../page_styles.css">
</head>

<body>
    <div class="container">
        <div class="lat_menu">
            <div class="menu_opt">
                <a href="../cartelera/cartelera_view.php" style="height:30px;"><img src="../../resource/svg/movie.svg"
                        height="30px"></a>
            </div>
            <div class="menu_opt_selected">
                <a href="#" style="height:30px;"><img src="../../resource/svg/food.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../reserva/reserva_view.php" style="height:30px;"><img src="../../resource/svg/register.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../cartelera_ad/cartelera_ad_view.php" style="height:30px;"><img src="../../resource/svg/admin.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <img src="../../resource/svg/log-out.svg" height="30px" onclick="logOut()">
            </div>
        </div>
        <div class="content">
            <h1>Venta de Comida</h1>
            <div class="snack_options">
                <div class="snack_button_selected" onclick="getSnacksByC('Combos', this)">Combos</div>
                <div class="snack_button" onclick="getSnacksByC('Bebidas', this)">Bebidas</div>
                <div class="snack_button" onclick="getSnacksByC('Comestibles', this)">Comestibles</div>
            </div>
            <div id="snacks">
                
            </div>
        </div>
        <div class="product_list">
            <h1>Consumo</h1>
            <div class="bill">
                <div class="bill_title">
                    <div style="font-size: 22px;">Orden #234</div>
                    <div class="print_button" style="font-size: 15px;" onclick="cleanBill()">Borrar todo</div>
                </div>
                <div class="bill_row" style="font-weight: bold; margin-bottom: 3px;">
                    <div style="width: 160px;">Producto</div>
                    <div class="cant_column">Cant.</div>
                    <div class="cant_column">Total</div>
                </div>
                <hr style="border-top: 1px solid black; margin-bottom: 10px;">
                <div id="bill_rows">
                    <div style="text-align: center;">Ningún producto agregado</div>
                </div>
            </div>
            <div class="input-wrapper">
                <label for="title" class="label_text_title">Nombre</label>
                <input type="text" name="title" class="input_text_title" id="nombre">
            </div>
            <div class="input-wrapper">
                <label for="title" class="label_text_title">C.I.</label>
                <input type="text" name="title" class="input_text_title" id="ci">
            </div>
            <div class="print_bill">
                <div>Total: <span style="font-size: 25px; color: var(--Red);" id="total_price">0Bs</span></div>
                <div class="print_button" onclick="sellSnacks()">Imprimir</div>
            </div>
        </div>
    </div>
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="snacks_script.js"></script>
<script src="bill_script.js"></script>
<script src="sell_snacks_script.js"></script>
<script src="stock_script.js"></script>
<script src="print_snack_bill_script.js"></script>
<script src="../login/login_script.js"></script>
<script>
    fetchSnacks('Combos')
</script>
</html>