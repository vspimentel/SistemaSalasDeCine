
// Define a class BillEntry that represents an entry in the bill
class BillEntry{
    // The constructor initializes the properties of the BillEntry object
    constructor(id, snack, cantidad, consumo, opt_products){
        this.id = id
        this.snack = snack
        this.cantidad = cantidad
        this.consumo = consumo
        this.opt_products = opt_products
    }
}

// Create an empty list to hold bill entries
bill_entry_list = []

// Define a function that creates a HTML row to display a bill entry
function createBillRow(bill_entry){
    return `<div class="bill_entry">
    <div class="bill_row">
        <div class="pm_button" onclick="removeProduct(${bill_entry.id})">-</div> 
        <div class="product_column">${bill_entry.snack.nombre}</div> 
        <div class="cant_column">${bill_entry.cantidad}</div> 
        <div class="cant_column">${bill_entry.cantidad*bill_entry.snack.precio}</div> 
    </div>
    <div class="entry_opts" id="entry_opts_${bill_entry.id}">
    </div>
</div>`
}

// Define a function that creates a HTML select element for a given option key and bill entry ID
function createSelect(key, bill_entry_id){
    return ` <div class="entry_opt"><div class="point"></div>
    <div class="entry_option">${key.slice(0, -1)} (${key[key.length-1]})</div> 
    <select id="${key}-${bill_entry_id}" class="input_select" onchange="updateConsumo(${bill_entry_id}, this)"> 
    </select></div>`
}

// Define a function that creates a HTML option element for a given option ID and name
function createOptions(id, option){
    return `<option value="${id}">${option}</option>` // Display the option name and ID as a value
}

// This function loads the select elements for the product options of a bill entry,
// given a bill entry id and an options list.
function loadSelects(bill_entry_id, opt_list){
    var list = document.getElementById("entry_opts_" + bill_entry_id)
    list.innerHTML = ""

    // Loop through each product option in the options list
    for(key in opt_list){
        // Create a new node for the select element
        new_node = document.createElement("div")
        new_node.innerHTML = createSelect(key, bill_entry_id)
        list.appendChild(new_node)

        // Get the select element for the current product option
        select = document.getElementById(`${key}-${bill_entry_id}`)

        // Loop through each option for the current product option
        for(key_o in opt_list[key]){
            // Add the option to the select element
            select.innerHTML += `<option value="${key_o}">${opt_list[key][key_o]}</option>`
        }
    }
}

// This function adds a new product to the bill or increases the quantity of an existing one.
function addProduct(snack_id){
    snack = snack_list.find(snack => snack.id == snack_id)

    // Get the stock of the product being added.
    stock = getStock(snack.productos, snack.cantList)

    // If the product is available in stock...
    if(stock > 0){

        // Find an existing bill entry for this product.
        bill_entry = bill_entry_list.find(bill_entry => bill_entry.id == snack_id)

        // If the bill entry exists, increase the quantity.
        if(bill_entry){
            bill_entry.cantidad++
            bill_entry = loadOptions(bill_entry, snack)
        } 

        // Otherwise, create a new bill entry for the product.
        else {
            snack = snack_list.find(snack => snack.id == snack_id)
            bill_entry = new BillEntry(snack.id, snack, 1, {}, {})
            bill_entry = loadOptions(bill_entry, snack) 
            bill_entry_list.push(bill_entry)
        }

        // Reduce the stock of the product in the inventory.
        snack.productos.forEach(p => {
            p.stock -= snack.cantList[p.tipo]
        });

        // Update the product list, price, and snacks.
        updateProductList()
        updatePrice()
        loadSnacks()
    }
}


// This function removes a product from the bill based on its snack ID
function removeProduct(snack_id){
    // Find the bill entry in the list of bill entries based on its snack ID
    bill_entry = bill_entry_list.find(bill_entry => bill_entry.id == snack_id)
    snack = bill_entry.snack

    // Increase the stock of each product in the snack by the quantity in the snack
    snack.productos.forEach(producto =>{
        producto.stock += snack.cantList[producto.tipo]
    })

    // If the bill entry has more than 1 of the snack, decrement the quantity by 1
    if(bill_entry.cantidad > 1){
        // Remove the consumption and optional products associated with the current quantity of the snack
        for(key in bill_entry.consumo){
            if(key.substr(key.length-1) == bill_entry.cantidad){
                delete bill_entry.consumo[key]
            }
        }
        for(key in bill_entry.opt_products){
            if(key.substr(key.length-1) == bill_entry.cantidad){
                delete bill_entry.opt_products[key]
            }
        }
        bill_entry.cantidad--
    } 
    // If the bill entry only has 1 of the snack, remove the entire bill entry from the list of bill entries
    else {
        i = bill_entry_list.findIndex(bill_entry => bill_entry.id == snack_id)
        bill_entry_list.splice(i, 1)
    }
    // Update the product list, the total price of the bill, and reload the snacks list
    updateProductList()
    updatePrice()
    loadSnacks()
}

function cleanBill(){
    // loop through each bill entry in the list
    bill_entry_list.forEach(entry =>{
        // get the snack for the entry
        snack = entry.snack
        // loop through each product in the snack
        snack.productos.forEach(producto =>{
            // add the quantity of the product back to the stock
            producto.stock += (snack.cantList[producto.tipo] * entry.cantidad)
        })
    })
    // reset the bill entry list
    bill_entry_list = []
    // update the product list and price
    updateProductList()
    updatePrice()
    // reload the snack list
    loadSnacks()
}

function updateConsumo(bill_entry_id, select){
    // Get the option key and bill entry object
    key = select.id.slice(0,-2)
    bill_entry = bill_entry_list.find(bill_entry => bill_entry.id == bill_entry_id)

    // Update the selected option in the consumption object of the bill entry
    bill_entry.consumo[key][0] = select.value
}

//This function loads the options for a given product in a bill entry and returns the updated bill entry
function loadOptions(bill_entry, snack){
    // Loop through each product in the snack object
    snack.productos.forEach(producto =>{
        // Initialize consumption object for the product in the bill entry
        bill_entry.consumo[producto.tipo + bill_entry.cantidad] = [Object.keys(producto.optList)[0], snack.cantList[producto.tipo]]

        // If there are more than one options for the product, load the options object into the bill entry's optional products object
        if(Object.keys(producto.optList).length > 1){
            bill_entry.opt_products[producto.tipo + bill_entry.cantidad] = producto.optList 
        }
    })
    // Return the updated bill entry
    return bill_entry
}

function updateProductList(){
    // Get the element that holds the bill rows
    var list = document.getElementById("bill_rows")
    // Clear the list
    list.innerHTML = ""
    // Loop through each bill entry and add a new row to the list
    bill_entry_list.forEach(bill_entry =>{
        // Create a new div node and set its innerHTML to the bill row HTML generated by createBillRow()
        new_node = document.createElement("div")
        new_node.innerHTML = createBillRow(bill_entry)
        // Append the new node to the bill rows list
        list.appendChild(new_node)
        // Load the select elements for the bill entry
        loadSelects(bill_entry.id, bill_entry.opt_products)
    })
    // If no products have been added to the bill, display a message
    if(bill_entry_list.length == 0)
        list.innerHTML = `<div style="text-align: center;">Ningún producto agregado</div>`
}