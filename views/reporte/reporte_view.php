<?php
    session_start();
    if(!isset($_SESSION["username"]) || $_SESSION["isadmin"] == 0){
        header("Location: ../unauthorized/unauthorized_view.html");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte</title>
    <link rel="stylesheet" href="../page_styles.css">
    <link rel="stylesheet" href="reporte_styles.css">
</head>
<body>
    <div class="container">
        <div class="lat_menu">
            <div class="menu_opt" >
                <a href="../cartelera_ad/cartelera_ad_view.php" style="height:30px;"><img src="../../resource/svg/movie.svg" height="30px"></a> 
            </div>
            <div class="menu_opt">
                <a href="../snacks_ad/snacks_ad_view.php" style="height:30px;"><img src="../../resource/svg/food.svg" height="30px"></a>
            </div>
            <div class="menu_opt_selected">
                <a href="#" style="height:30px;"><img src="../../resource/svg/stats.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../user_list/user_list_view.php" style="height:30px;"><img src="../../resource/svg/user_list.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../cartelera/cartelera_view.php" style="height:30px;"><img src="../../resource/svg/user1.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <img src="../../resource/svg/log-out.svg" height="30px" onclick="logOut()">
            </div>
        </div>
        <div class="content_report">
            <h1>Reporte</h1>
            <div class="report_settings">
                <div class="report_options">
                    <div class="report_button_selected" onclick="getReportByOpt('Boletos', this)">
                    <a href="#" style="color:black;">Boletos</a></div>
                    <div class="report_button" onclick="getReportByOpt('Snacks', this)">
                    <a href="reporte_snacks.php" style="color:white;">Snacks</a></div>
                    <div class="report_button" onclick="getReportByOpt('Total', this)">
                    Total</div>
                </div>
                <div class="report_date">
                <form method="POST">
                    <div class="date_picker">
                        <div class="date_picker_title">Desde</div>
                        <input type="date" name="date1" id="date1" style="border-radius: 4px; padding-left: 3px;border-color: white;" value="<?php if(isset($_POST['date1'])) echo $_POST['date1']; ?>">
                    </div>
                    <div class="date_picker">
                        <div class="date_picker_title">Hasta</div>
                        <input type="date" name="date2" id="date2" style="border-radius: 4px; padding-left: 3px;border-color: white;" value="<?php if(isset($_POST['date2'])) echo $_POST['date2']; ?>">
                    </div>
                    <div class="div_button">
                        <button class="button_generate" type="submit" name="submit"> Generar </button>
                    </div>
                </form>
                </div>
            </div>
            
            <div class="content" id="content">
                <div class="report_container" id="report_container">

                    <?php
                        include("../../db/connect.php");
                        $table = array();
                        $total = 0;
                        if(isset($_POST['submit'])){
                            $start_date = $_POST['date1'];
                            $end_date = $_POST['date2'];
                            $sql = "SELECT * FROM `venta`";
                            $sql = "SELECT V.FECHA, P.NOMBRE, P.TIPO, V.HORARIO, V.PRECIO, V.DIA, COUNT(*), SUM(PRECIO) 
                            FROM VENTA V INNER JOIN PELICULA P ON V.IDPELI = P.IDPELI 
                            WHERE FECHA >= '$start_date 00:00:00' AND FECHA <= '$end_date 23:59:59' 
                            GROUP BY V.IDPELI, V.HORARIO;";
                            $statement = $con->prepare($sql);
                            $statement->execute();
                            $result = $statement->get_result();
                        
                            while($row = mysqli_fetch_assoc($result))
                            {
                                $table[] = $row;
                                $total += $row['SUM(PRECIO)'];
                            }
                        }
                    ?>
                    <table>
                        <tr>
                        <th style="width:150px">Fecha</th>
                        <th style="width:140px">Día</th>
                        <th style="width:280px">Película</th>
                        <th style="width:90px">Tipo</th>
                        <th style="width:120px">Horario </th>
                        <th style="width:120px">Precio (Bs)</th>
                        <th style="width:100px">Cantidad</th>
                        <th style="width:160px">Ganancia (Bs)</th>
                        </tr>

                        <?php foreach ($table as $row): ?>
                        <tr>
                            <td><?= substr($row['FECHA'], 0, 10)?></td>
                            <td><?= $row['DIA'] ?></td>
                            <td><?= $row['NOMBRE'] ?></td>
                            <td><?= $row['TIPO'] ?></td>
                            <td><?= $row['HORARIO'] ?></td>
                            <td><?= $row['PRECIO'] ?></td>
                            <td><?= $row['COUNT(*)'] ?></td>
                            <td><?= $row['SUM(PRECIO)'] ?></td>
                        </tr>
                        <?php endforeach; ?>
                    <?php $con->close(); ?>
                        <tr>
                            <th style="border-bottom: 0">Total (Bs)</th>
                            <td style="border-bottom: 0"></td>
                            <td style="border-bottom: 0"></td>
                            <td style="border-bottom: 0"></td>
                            <td style="border-bottom: 0"></td>
                            <td style="border-bottom: 0"></td>
                            <td style="border-bottom: 0"></td>
                            <th style="border-bottom: 0"><?= $total ?></th>
                        </tr>
                        
                    </table> 
                </div>

                <?php
                    include("../../db/connect.php");
                    $table = array();
                    $total = 0;
                    if(isset($_POST['submit'])){
                        $start_date = $_POST['date1'];
                        $end_date = $_POST['date2'];
                        $sql = "SELECT * FROM `venta`";
                        $sql = "SELECT V.FECHA, P.NOMBRE, V.PRECIO, COUNT(*), SUM(PRECIO) 
                        FROM VENTA V INNER JOIN PELICULA P ON V.IDPELI = P.IDPELI 
                        WHERE FECHA >= '$start_date 00:00:00' AND FECHA <= '$end_date 23:59:59' 
                        GROUP BY V.IDPELI;";
                        $statement = $con->prepare($sql);
                        $statement->execute();
                        $result = $statement->get_result();
                    
                        while($row = mysqli_fetch_assoc($result))
                        {
                            $table[] = $row;
                        }
                    }
                    $json_table = json_encode($table);
                ?>
                <script>
                    var jsonData = <?php echo $json_table; ?>;
                </script>

                <div class="button_generate" id="button_generate" onclick="generateCharts()">Generar gráficos</div>
                <div class="chart_titles" id="chart_titles">
                    <div class="chart_title1">Ganancias por película</div>
                    <div class="chart_title2">Boletos vendidos por película</div>
                </div>
    
                <div class="charts" id="charts">
                    
                    <div class="chart1" id="c1">
                        
                        <canvas id="chart1"></canvas>
                    </div>
                    <div class="chart2" id="c2"><canvas id="chart2"></canvas></div>
                </div>
            </div>       
          
        </div>
        
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="report_script.js"></script>
    <script src="../login/login_script.js"></script>
</body>
<script>
    //setCurDate()
</script>
</html>