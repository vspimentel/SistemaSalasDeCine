var content = document.getElementById("content");

const ctx = document.getElementById('chart1');
const ctx2 = document.getElementById('chart2');
const ctx3 = document.getElementById('chart3');
const ctx4 = document.getElementById('chart4');
const ctx5 = document.getElementById('chart5');
const ctx6 = document.getElementById('chart6');
var c1 = document.getElementById("c1");
var c2 = document.getElementById("c2");
var c3 = document.getElementById("c3");
var c4 = document.getElementById("c4");
var c5 = document.getElementById("c5");
var c6 = document.getElementById("c6");
var titles = document.getElementById("chart_titles");
var jsonData = jsonData; // json data from boletos
var jsonData2 = jsonData2; // json data from snacks
var jsonData3 = jsonData3; // json data from tickets

// Convert the JSON data into separate arrays for use with Chart.js
var nombres = [];
var precios = [];
var cantidades = [];
var ganancias = [];


// convert json data to arrays from snacks
var nombres2 = [];
var precios2 = [];
var cantidades2 = [];
var ganancias2 = [];

var nombres3 = ["Boletos", "Snacks"];
var ganancias3 = ["3270","2528"];

function generateCharts(){

  for (var i = 0; i < jsonData.length; i++) {
    nombres.push(jsonData[i]['NOMBRE']);
    precios.push(jsonData[i]['PRECIO']);
    cantidades.push(jsonData[i]['COUNT(*)']);
    ganancias.push(jsonData[i]['SUM(PRECIO)']);
}
  c1.style.backgroundColor = "white";
  titles.style.color = "white"; 

  new Chart(ctx, {
    type: 'bar',
    data: {
      labels: nombres,
      datasets: [{
        label: 'Ganancia por Película (Bs.)',
        data: ganancias,
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });
  
  c2.style.backgroundColor = "white";
new Chart(ctx2, {
    type: 'doughnut',
    data: {
      labels: nombres,
      datasets: [{
        label: 'Boletos vendidos',
        data: cantidades,
        backgroundColor: [
          'rgb(75, 192, 192)',
          'rgb(153, 102, 255)',
          'rgb(255, 159, 64)',
          'rgb(255, 99, 132)',
          'rgb(54, 162, 235)',
          'rgb(255, 205, 86)',
        ],
        hoverOffset: 4
      }]
    }
  });
}

function generateCharts2(){
  for (var i = 0; i < jsonData2.length; i++) {
    nombres2.push(jsonData2[i]['NOMBRE']);
    precios2.push(jsonData2[i]['V.PRECIO']);
    cantidades2.push(jsonData2[i]['COUNT(*)']);
    ganancias2.push(jsonData2[i]['SUM(V.PRECIO)']);
}
  titles.style.color = "white"; 
  c3.style.backgroundColor = "white";
  new Chart(ctx3, {
    type: 'bar',
    data: {
      labels: nombres2,
      datasets: [{
        label: 'Ganancia por Snack (Bs.)',
        data: ganancias2,
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });

  c4.style.backgroundColor = "white";
  new Chart(ctx4, {
    type: 'doughnut',
    data: {
      labels: nombres2,
      datasets: [{
        label: 'Snacks vendidos',
        data: cantidades2,
        backgroundColor: [
          'rgb(75, 192, 192)',
          'rgb(153, 102, 255)',
          'rgb(255, 159, 64)',
        ],
        hoverOffset: 4
      }]
    }
  });

}

function generateCharts3(){
//   for (var i = 0; i < jsonData3.length; i++) {

//     ganancias3.push(jsonData3[i]['SUM(V.PRECIO)']);
//     ganancias3.push(jsonData3[i]['SUM(VS.PRECIO)']);
// }
  titles.style.color = "white"; 
  c5.style.backgroundColor = "white";
  new Chart(ctx5, {
    type: 'bar',
    data: {
      labels: nombres3,
      datasets: [{
        label: 'Ganancias Totales(Bs.)',
        data: ganancias3,
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });

  c6.style.backgroundColor = "white";
  new Chart(ctx6, {
    type: 'doughnut',
    data: {
      labels: nombres3,
      datasets: [{
        label: 'Ganancias Totales',
        data: ganancias3,
        backgroundColor: [
          'rgb(75, 192, 192)',
          'rgb(153, 102, 255)',
          'rgb(255, 159, 64)',
        ],
        hoverOffset: 4
      }]
    }
  });
}

  var report_container = document.getElementById("report_container");
  var date1 = document.getElementById("date1").value;
  var date2 = document.getElementById("date2").value;
  var charts = document.getElementById("charts");
  var button_generate = document.getElementById("button_generate");

  function fetchSale() 
  {
    fetch("../../db/connect.php")

  }
      
  function getReportByDate(){
    fetchSale()
  }

  function getReportByOpt(opt, button){
    //fetchSale()
    last_button = document.getElementsByClassName("report_button_selected")[0]
    last_button.className = "report_button"
    button.className = "report_button_selected"
    // if(opt == 'Boletos'){
    //   //display a reporte_tickets.php file on the content div
    //   fetch("../../views/reporte/reporte_tickets.php?date1="+date1+"&date2="+date2)
    //   .then(response => response.text())
    //   .then(data => content.innerHTML = data)
    // }
  }





