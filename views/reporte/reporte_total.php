<?php
    session_start();
    if(!isset($_SESSION["username"]) || $_SESSION["isadmin"] == 0){
        header("Location: ../unauthorized/unauthorized_view.html");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte</title>
    <link rel="stylesheet" href="../page_styles.css">
    <link rel="stylesheet" href="reporte_styles.css">
</head>
<body>
    <div class="container">
        <div class="lat_menu">
            <div class="menu_opt" >
                <a href="../cartelera_ad/cartelera_ad_view.php" style="height:30px;"><img src="../../resource/svg/movie.svg" height="30px"></a> 
            </div>
            <div class="menu_opt">
                <a href="../snacks_ad/snacks_ad_view.php" style="height:30px;"><img src="../../resource/svg/food.svg" height="30px"></a>
            </div>
            <div class="menu_opt_selected">
                <a href="#" style="height:30px;"><img src="../../resource/svg/stats.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../user_list/user_list_view.php" style="height:30px;"><img src="../../resource/svg/user_list.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../cartelera/cartelera_view.php" style="height:30px;"><img src="../../resource/svg/user1.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <img src="../../resource/svg/log-out.svg" height="30px" onclick="logOut()">
            </div>
        </div>
        <div class="content_report">
            <h1>Reporte</h1>
            <div class="report_settings">
                <div class="report_options">
                    <div class="report_button" onclick="getReportByOpt('Boletos', this)">
                    <a href="reporte_view.php" style="color:white;">Boletos</a></div>
                    <div class="report_button" onclick="getReportByOpt('Snacks', this)">
                    <a href="reporte_snacks.php" style="color:white;">Snacks</a></div>
                    <div class="report_button_selected" onclick="getReportByOpt('Total', this)">
                    <a href="#" style="color:black;">Total</a></div>
                </div>
                <div class="report_date">
                <form method="POST">
                    <div class="date_picker">
                        <div class="date_picker_title">Desde</div>
                        <input type="date" name="date1" id="date1" style="border-radius: 4px; padding-left: 3px;border-color: white;" value="<?php if(isset($_POST['date1'])) echo $_POST['date1']; ?>">
                    </div>
                    <div class="date_picker">
                        <div class="date_picker_title">Hasta</div>
                        <input type="date" name="date2" id="date2" style="border-radius: 4px; padding-left: 3px;border-color: white;" value="<?php if(isset($_POST['date2'])) echo $_POST['date2']; ?>">
                    </div>
                    <div class="div_button">
                        <button class="button_generate" type="submit" name="submit"> Generar </button>
                    </div>
                </form>
                </div>
            </div>
            
            <div class="content" id="content">
                <div class="report_container" id="report_container">

                    <?php
                        include("../../db/connect.php");
                        $table = array();
                        $total = 0;
                        $totalV = 0;
                        $totalVS = 0;
                        if(isset($_POST['submit'])){
                            $start_date = $_POST['date1'];
                            $end_date = $_POST['date2'];
                            $sql = "SELECT V.FECHA, VS.FECHA, V.PRECIO, VS.PRECIO, SUM(V.PRECIO), SUM(VS.PRECIO)
                            FROM VENTA V INNER JOIN VENTASNACK VS ON DATE(V.FECHA) = DATE(VS.FECHA)
                            WHERE V.FECHA >= '$start_date 00:00:00' AND V.FECHA <= '$end_date 23:59:59'";
                            $statement = $con->prepare($sql);
                            $statement->execute();
                            $result = $statement->get_result();
                            if(mysqli_num_rows($result) > 0) {
                                while($row = mysqli_fetch_assoc($result)) {
                                    $table[] = $row;
                                    $totalV += $row['SUM(V.PRECIO)'];
                                    $totalVS += $row['SUM(VS.PRECIO)'];
                                }
                              }
                            

                        }
                    ?>
                    <table>
                        <tr>
                        <th style="width:150px">Fecha</th>
                        <th style="width:200px">Ganancia Boletos (Bs)</th>
                        <th style="width:200px">Ganancia Snack (Bs)</th>
                        <th style="width:200px">Ganancia Total (Bs)</th>
                        </tr>

                        <?php foreach ($table as $row): ?>
                        <tr>
                            <td><?= substr($row['FECHA'], 0, 10)?></td>
                            <td><?= $row['SUM(V.PRECIO)'] ?></td>
                            <td><?= $row['SUM(VS.PRECIO)'] ?></td>
                            <td><?= $row['SUM(V.PRECIO)'] + $row['SUM(VS.PRECIO)'] ?></td>
                        </tr>
                        <?php endforeach; ?>
                    <?php $con->close(); ?>
                        <tr>
                            <th style="border-bottom: 0">Total (Bs)</th>
                            <td style="border-bottom: 0"></td>
                            <td style="border-bottom: 0"></td>
                            <th style="border-bottom: 0"><?= $totalV + $totalVS ?></th>
                        </tr>
                        
                    </table> 
                </div>

                <?php
                    include("../../db/connect.php");
                    $table = array();
                    if(isset($_POST['submit'])){
                        $start_date = $_POST['date1'];
                        $end_date = $_POST['date2'];
                       
                        $sql = "SELECT V.FECHA, VS.FECHA, V.PRECIO, VS.PRECIO, SUM(V.PRECIO), SUM(VS.PRECIO)
                        FROM VENTA V INNER JOIN VENTASNACK VS ON V.FECHA = VS.FECHA
                        WHERE V.FECHA >= '$start_date 00:00:00' AND V.FECHA <= '$end_date 23:59:59'";
                        $statement = $con->prepare($sql);
                        $statement->execute();
                        $result = $statement->get_result();
                    
                        while($row = mysqli_fetch_assoc($result))
                        {
                            $table[] = $row;
                        }
                    }
                    $json_table = json_encode($table);
                ?>
                <script>
                    var jsonData3 = <?php echo $json_table; ?>;
                </script>

                <div class="button_generate" id="button_generate" onclick="generateCharts3()">Generar gráficos</div>
                <div class="chart_titles" id="chart_titles">
                    <div class="chart_title1">Ganancias Totales</div>
                    <div class="chart_title2">Ganancias Boletos Snacks</div>
                </div>
    
                <div class="charts" id="charts">
                    
                    <div class="chart5" id="c5"> 
                        <canvas id="chart5"></canvas>
                    </div>
                    <div class="chart6" id="c6"><canvas id="chart6"></canvas></div>
                </div>
            </div>       
          
        </div>
        
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="report_script.js"></script>
    <script src="../login/login_script.js"></script>
</body>
<script>
    //setCurDate()
</script>
</html>