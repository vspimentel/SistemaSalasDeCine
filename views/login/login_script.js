// Function to display error message
function showError(message) {
    error.style.display = "block";
    password_wraper.style.marginBottom = "0px";
    error.innerHTML = message;
}

// Function to hide error message
function hideError() {
    error.style.display = "none";
    password_wraper.style.marginBottom = "29px";
}

// Function to get user data and send it to server for authentication
function getUserData() {
    // Get username and password from input fields
    username = document.getElementById("username").value;
    password = document.getElementById("password").value;
    // Create a new FormData object to send data to server
    var datos = new FormData();
    datos.append("username", username);
    datos.append("password", password);
    // Send data to server using fetch API
    fetch("../../db/fetchQuery.php?query=user", { method: "POST", body: datos })
        .then(response => response.text())
        .then(data => {
            // Parse server response to JSON format
            response = JSON.parse(data);
            // If user exists in database, login and redirect to main page
            if (response.length) {
                username = response[0]["USERNAME"]
                isadmin = response[0]["ISADMIN"]
                login(username, isadmin, false);
            } else { // If user doesn't exist, show error message
                showError("Usuario o contraseña incorrectos");
            }
        });
}

// Function to get admin data and send it to server for authentication
function getAdminData() {
    // Get username and password from input fields
    username = document.getElementById("username").value;
    password = document.getElementById("password").value;
    // Create a new FormData object to send data to server
    var datos = new FormData();
    datos.append("username", username);
    datos.append("password", password);
    // Send data to server using fetch API
    fetch("../../db/fetchQuery.php?query=user", { method: "POST", body: datos })
        .then(response => response.text())
        .then(data => {
            // Parse server response to JSON format
            response = JSON.parse(data);
            // If user exists in database and is admin, login and redirect to admin page
            if (response.length) {
                username = response[0]["USERNAME"]
                isadmin = response[0]["ISADMIN"]
                if (isadmin == "Si") {
                    login(username, isadmin, true);
                } else { // If user is not authorized, show error message
                    showError("Usuario no autorizado");
                }
            } else { // If user doesn't exist, show error message
                showError("Usuario o contraseña incorrectos");
            }
        });
}

// Function to start a user session and redirect to main or admin page
function login(username, isadmin, asadmin) {
    // Create a new FormData object to send data to server
    var datos = new FormData();
    datos.append("username", username);
    datos.append("isadmin", isadmin);
    // Send data to server using fetch API
    fetch("../../db/sessions.php?action=startSession", { method: "POST", body: datos })
        .then(response => response.text())
        .then(data => {
            if (asadmin) { // If user is admin, redirect to admin page
                location = "../cartelera_ad/cartelera_ad_view.php";
            } else { // If user is not admin, redirect to main page
                location = "../cartelera/cartelera_view.php";
            }
        });
}


// This function logs out the user by ending the session and redirecting them to the login page
function logOut() {
    fetch("../../db/sessions.php?action=endSession")
        .then(response => response.text())
        .then(data => {
            location.href = "../login/login_view.html"
        });
}