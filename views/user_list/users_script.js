class User {
    constructor(id, username, name, phone, ci, charge, isadmin, lastlogin) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.phone = phone;
        this.ci = ci;
        this.charge = charge;
        this.isadmin = isadmin;
        this.lastlogin = lastlogin;
    }
}

user_list = [];

async function fetchUsers(search) {
    try {
        var url = "../../db/fetchQuery.php?query=users"
        if (search != undefined) {
            url += `&search=${search}`
        }
        const response = await fetch(url);
        const data = await response.json();
        if (data.length) {
            user_list = [];
            data.forEach((value) => {
                var isadmin = value.ISADMIN == "Si";
                var date = new Date(value.LASTLOGIN);
                var lastlogin = date.toLocaleDateString() + " " + date.toLocaleTimeString();
                user_list.push(new User(value.ID, value.USERNAME, value.NAME, value.PHONE, value.CI, value.CHARGE, isadmin, lastlogin));
            })
            loadUsers();
        }
    } catch (error) {
        console.error("Error fetching users:", error);
    }
}

function createUser(user) {
    var u = `<div class="user">
            <div class="user_row">
            <div style="display: flex; gap: 10px; align-items: center;">
            <div class="name">${user.username}</div>`
    if (user.isadmin) {
        u += `<div class="tag">Administrador</div>`
    }
    u += `</div>
                <div class="last_conect">Ult. conexión: ${user.lastlogin}</div>
            </div>
            <div class="user_row">${user.name} | Cel. ${user.phone}</div>
    <div class="user_row">
        <div>CI: ${user.ci} | Cargo: ${user.charge}</div>
        <div style="display: flex; gap: 10px;">
            <div class="user_button" onclick="deleteUser(${user.id})">
                <img src="../../resource/svg/delete.svg" height="15px">
                Borrar
            </div>
            <div class="user_button" onclick="loadForm(${user.id})">
                <img src="../../resource/svg/edit.svg" height="15px">
                Editar
            </div>
        </div>
    </div>
        </div>`
    return u;
}

function loadUsers() {
    var list = document.getElementById("user_list")
    list.innerHTML = ""
    user_list.forEach(user => {
        new_node = document.createElement("div")
        new_node.innerHTML = createUser(user)
        list.appendChild(new_node)
    });
}

var search = document.getElementById("search");
search.addEventListener("click", () => {
    var input = document.getElementById("search_input").value;
    fetchUsers(input);
})