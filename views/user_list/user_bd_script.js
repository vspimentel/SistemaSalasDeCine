var id = document.getElementById("id");
var username = document.getElementById("username");
var password = document.getElementById("password");
var fname = document.getElementById("name");
var ci = document.getElementById("ci");
var charge = document.getElementById("charge");
var phone = document.getElementById("phone");
var isadmin = document.getElementById("isadmin");
var formTitle = document.getElementById("form_title");
var user_button = document.getElementById("user_button");

function insertUser() {
    var datos = new FormData();
    datos.append("username", username.value);
    datos.append("password", password.value);
    datos.append("name", fname.value);
    datos.append("ci", ci.value);
    datos.append("charge", charge.value);
    datos.append("phone", phone.value);
    if (isadmin.checked) {
        datos.append("isadmin", "Si");
    } else {
        datos.append("isadmin", "No");
    }
    fetch("../../db/insertUser.php", { method: "POST", body: datos })
        .then(response => response.text())
        .then(data => {
            fetchUsers()
            cleanForm()
        });
}

function editUser() {
    var datos = new FormData();
    datos.append("id", id.value);
    datos.append("username", username.value);
    datos.append("password", password.value);
    datos.append("name", fname.value);
    datos.append("ci", ci.value);
    datos.append("charge", charge.value);
    datos.append("phone", phone.value);
    if (isadmin.checked) {
        datos.append("isadmin", "Si");
    } else {
        datos.append("isadmin", "No");
    }
    fetch("../../db/editUser.php", { method: "POST", body: datos })
        .then(response => response.text())
        .then(data => {
            fetchUsers()
            cleanForm()
        });
}

function deleteUser(id_user) {
    user = user_list.find(user => user.id == id_user);
    if (confirm(`Desea eliminar el usuario ${user.username}`)) {
        var datos = new FormData();
        datos.append("id", id_user);
        fetch("../../db/deleteUser.php", { method: "POST", body: datos })
            .then(response => response.text())
            .then(data => {
                fetchUsers()
            });
    }
}

function cleanForm() {
    id.value = "";
    username.value = "";
    password.value = "";
    fname.value = "";
    ci.value = "";
    charge.value = "";
    phone.value = "";
    isadmin.checked = false;
    formTitle.innerHTML = "Crear usuario";
    user_button.innerHTML = "Registrar";
    user_button.onclick = insertUser;
}

function loadForm(id_user) {
    var user = user_list.find(user => user.id == id_user);
    id.value = user.id;
    username.value = user.username;
    fname.value = user.name;
    ci.value = user.ci;
    charge.value = user.charge;
    phone.value = user.phone;
    if (user.isadmin) {
        isadmin.checked = true;
    } else {
        isadmin.checked = false;
    }
    formTitle.innerHTML = "Editar usuario";
    user_button.innerHTML = "Editar";
    user_button.onclick = editUser;
}
