<?php
    session_start();
    if(!isset($_SESSION["username"]) || $_SESSION["isadmin"] == 0){
        header("Location: ../unauthorized/unauthorized_view.html");
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de usuarios</title>
    <link rel="stylesheet" href="../page_styles.css">
    <link rel="stylesheet" href="user_list_styles.css">
</head>

<body>
    <div class="container">
        <div class="lat_menu">
            <div class="menu_opt">
                <a href="../cartelera_ad/cartelera_ad_view.php" style="height:30px;"><img
                        src="../../resource/svg/movie.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="#" style="height:30px;"><img src="../../resource/svg/food.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../reporte/reporte_view.php" style="height:30px;"><img src="../../resource/svg/stats.svg"
                        height="30px"></a>
            </div>
            <div class="menu_opt_selected">
                <a href="#" style="height:30px;"><img src="../../resource/svg/user_list.svg" height="30px"></a>
            </div>
            <div class="menu_opt">
                <a href="../cartelera/cartelera_view.php" style="height:30px;"><img src="../../resource/svg/user1.svg"
                        height="30px"></a>
            </div>
            <div class="menu_opt">
                <img src="../../resource/svg/log-out.svg" height="30px" onclick="logOut()">
            </div>
        </div>
        <div class="content">
            <h1>Gestionar usuarios</h1>
            <div style="display: flex; justify-content: space-between;">
                <div style="display: flex; gap: 20px;">
                    <div class="button" id="search">Buscar usuario</div>
                    <input type="text" placeholder="Username, nombre o CI" id="search_input">
                </div>
                <div class="button" onclick="cleanForm()">Crear usuario</div>
            </div>
            <div id="user_list">
                <div class="user">

                </div>
            </div>
        </div>
        <div class="edit_form">
            <h1 style="font-size: 25px;" id="form_title">Crear usuario</h1>
            <input type="hidden" id="id">
            <div class="input-wrapper">
                <label for="username" class="label_text">Nombre de usuario</label>
                <input type="text" name="username" class="input_text" id="username">
            </div>
            <div class="input-wrapper">
                <label for="password" class="label_text">Contraseña</label>
                <input type="password" name="password" class="input_text" id="password">
            </div>
            <div class="input-wrapper">
                <label for="name" class="label_text">Nombre completo</label>
                <input type="text" name="name" class="input_text" id="name">
            </div>
            <div class="input-wrapper">
                <label for="ci" class="label_text">CI</label>
                <input type="text" name="ci" class="input_text" id="ci">
            </div>
            <div class="input-wrapper">
                <label for="charge" class="label_text">Cargo</label>
                <input type="text" name="charge" class="input_text" id="charge">
            </div>
            <div class="input-wrapper">
                <label for="phone" class="label_text">Celular</label>
                <input type="text" name="phone" class="input_text" id="phone">
            </div>
            <div style="display: flex; justify-content: space-between;">
                <label class="radio_label">
                    <input type="checkbox" name="admin" id="isadmin"/>
                    Administrador
                </label>
                <div class="user_button" id="user_button" onclick="insertUser()">Registrar</div>
            </div>
        </div>
    </div>
    <script src="../login/login_script.js"></script>
    <script src="users_script.js"></script>
    <script src="user_bd_script.js"></script>
    <script>
        fetchUsers();
    </script>
</body>
</html>