<?php
    session_start();
    if(isset($_SESSION["username"]) && $_SESSION["isadmin"] == 1){
        include("connect.php");
        $id = $_POST['id'];
        $nombre = $_POST['nombre'];
        $image_name = $_FILES["poster"]["name"];
        //$oldimagen = $_POST["posterOld"];
        $tipo = $_POST['video'];
        $audio = $_POST['audio'];
        $horario = $_POST['horario'];
        //unlink("../resource/".$oldimagen);
        //copy($newimagen, "images/".$image_name);
        $updateMovie = "UPDATE PELICULA SET NOMBRE = '$nombre', POSTER = '$image_name', TIPO = '$tipo', AUD = '$audio' WHERE IDPELI = $id";
        mysqli_query($con, $updateMovie);
        $horarioArray = explode("|", $horario);
        $horarioArray = array_map(function($item) {
            return explode("-", $item);
        }, $horarioArray); 
        $fetchSchedule = "SELECT * FROM `PELICULA-SALA` ps INNER JOIN SALA s ON s.IDSALA = ps.IDSALA WHERE IDPELI = $id";
        $result = mysqli_query($con, $fetchSchedule);
        $schedules = array();
        while($row = mysqli_fetch_assoc($result)) {
            $schedule = [$row["HORARIO"], $row["NOMBRE_SALA"]];
            if(!in_array($schedule, $horarioArray)) {
                $deleteSchedule = "DELETE FROM `PELICULA-SALA` WHERE IDFUNCION = $row[IDFUNCION]";
                mysqli_query($con, $deleteSchedule);
                $deleteSeats = "DELETE FROM `ASIENTO` WHERE IDFUNCION = $row[IDFUNCION]";
                mysqli_query($con, $deleteSeats);
            } else {
                $schedules[] = $schedule;
            }
        }
        foreach($horarioArray as $item) {
            if(!in_array($item, $schedules)) {
                $insertSchedule = "INSERT INTO `PELICULA-SALA`(IDPELI,  IDSALA, HORARIO) VALUES";
                $insertSchedule .= "((SELECT IDPELI FROM PELICULA WHERE NOMBRE = '$nombre'), (SELECT IDSALA FROM SALA WHERE NOMBRE_SALA = '$item[1]'), '$item[0]')";
                mysqli_query($con, $insertSchedule);
                $fetchHall = "SELECT * FROM SALA WHERE NOMBRE_SALA = '$item[1]'";
                $result = mysqli_query($con, $fetchHall);
                $row = mysqli_fetch_assoc($result);
                $id_sala = $row["IDSALA"];
                $rows = $row["FILAS"];
                $cols = $row["COLUMNAS"];
                $fetcSchedule = "SELECT IDFUNCION FROM `PELICULA-SALA` WHERE IDSALA = $id_sala AND HORARIO = '$item[0] AND IDPELI = (SELECT IDPELI FROM PELICULA WHERE NOMBRE = '$nombre')";
                $result = mysqli_query($con, $fetcSchedule);
                $row = mysqli_fetch_assoc($result);
                $id = $row["IDFUNCION"];
                $insertSeats = "INSERT INTO `ASIENTO`(IDFUNCION) VALUES";
                for($i = 0; $i < $rows*$cols; $i++) {
                    $insertSeats .= "($id)";
                    if($i != $rows - 1) {
                        $insertSeats .= ",";
                    }
                }
                mysqli_query($con, $insertSeats);
            }
        }
    }
?>