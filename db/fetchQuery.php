<?php
    include("connect.php");
    $query = $_GET["query"];
    switch ($query) {
        case 'movies':
            $sql = "SELECT * FROM `pelicula-sala` ps INNER JOIN pelicula p ON p.IDPELI = ps.IDPELI INNER JOIN sala s ON s.IDSALA = ps.IDSALA;";
            break;
        case 'price':
            $sql = "SELECT * FROM `dia`";
            break;
        case 'lastSell':
            $sql = "SELECT MAX(IDVENTA) AS IDVENTA FROM VENTA;";
            break;
        case 'schedule':
            $id_funcion = $_GET['id'];
            $sql = "SELECT * FROM `PELICULA-SALA` ps INNER JOIN SALA s ON s.IDSALA = ps.IDSALA WHERE IDFUNCION = $id_funcion";
            break;
        case 'tickets':
            $ci = $_GET["ci"];
            $sql = "SELECT P.NOMBRE, P.TIPO, P.AUD, V.IDVENTA, V.HORARIO, V.ASIENTO, V.SALA, V.DIA, V.FECHA, C.CI, C.NOMBRE AS C_NOMBRE FROM VENTA V INNER JOIN PELICULA P ON V.IDPELI = P.IDPELI INNER JOIN COMPRADOR C ON V.IDCOMPRADOR = C.IDCOMPRADOR WHERE C.CI = '$ci'";
            break;
        case 'seats':
            $day = $_GET["day"];
            $idfuncion = $_GET["idfuncion"];
            $days = array("DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO");
            $sql = "SELECT IDBUTACA, $days[$day] as ESTADO FROM `asiento` WHERE IDFUNCION = $idfuncion";
            break;
        case 'snacks':
            $categoria = $_GET["categoria"];
            $sql = "SELECT id_combo, id_producto, c.nombre AS c_nombre, p.nombre AS p_nombre, imagen, cp.cantidad AS cant_combo, tipo, unidad, p.cantidad AS cant_stock, descripcion, precio FROM `combo-producto` cp INNER JOIN producto p ON p.id = cp.id_producto INNER JOIN combo c ON c.id = cp.id_combo WHERE categoria = '$categoria';";
            break;
        case 'reports':
            $start_date = $_GET["start"];
            $end_date = $_GET["end"];
            $sql = "SELECT * FROM REGISTROS R INNER JOIN PRODUCTO P ON R.IDPRODUCTO = P.id WHERE HORA >= '$start_date 00:00:00' AND HORA <= '$end_date 23:59:59' ORDER BY HORA DESC";
            break;
        case 'productRep':
            $id = $_GET["id"];
            $sql = "SELECT * FROM REGISTROS WHERE IDPRODUCTO = $id ORDER BY HORA DESC LIMIT 5 ";
            break;
        case 'products':
            $sql = "SELECT * FROM PRODUCTO";
            break;
        case 'user':
            $username  = $_POST["username"];
            $password = $_POST["password"];
            $sql = "SELECT * FROM USUARIOS WHERE USERNAME = '$username' AND PASSWORD = SHA1('$password')";
            break;
        case 'users':
            $sql = "SELECT * FROM USUARIOS";
            if(isset($_GET["search"]))
            {
                $search = $_GET["search"];
                $sql .= " WHERE NAME LIKE '%$search%' OR USERNAME LIKE '%$search%' OR CI LIKE '%$search%'";
            }
            $sql .= " ORDER BY LASTLOGIN DESC LIMIT 10";
            break;
        default:
            break;
    }
    $statement = $con->prepare($sql);
    $statement->execute();
    $result = $statement->get_result();
    $table = array();
    while($row = mysqli_fetch_assoc($result))
    {
        $table[] = $row;
    }
    echo json_encode($table);
?>